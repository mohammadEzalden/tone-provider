class User {
  String id;
  String name;
  String phone;
  String email;
  String imageUrl;

  User({this.id, this.name, this.phone, this.email, this.imageUrl});

  factory User.fromJson(Map<String, dynamic> map) {
    return User(
      id: map['_id'] ?? "",
      name: map['name'] ?? "",
      phone: map['phone'] ?? "",
      email: map['email'] ?? "",
      imageUrl: map['imageUrl'] ?? "",
    );
  }

  Map<String, dynamic> toJson() => {
        '_id': id ?? "",
        'name': name,
        'phone': phone,
        'email': email,
        'imageUrl': imageUrl ?? "",
      };
}
