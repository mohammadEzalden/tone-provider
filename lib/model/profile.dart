class Profile {
  String id;
  String name;
  String phone;
  String email;

  Profile(
      {this.id,
        this.name,
        this.phone,
        this.email,});

  factory Profile.fromJson(Map<String, dynamic> map) {
    return Profile(
      id: map['_id'] ?? "",
      name: map['name'] ?? " ",
      phone: map['imageUrl'] ?? " ",
      email: map['address'] ?? "",
    );
  }

  Map<String, dynamic> toJson() => {
    'name': name,
    'address': email,
    'imageUrl': phone,
  };
}
