class Seat {
  String type;
  int price;
  int number;

  Seat({this.price, this.type, this.number});

  factory Seat.fromJson(Map<String, dynamic> map) {
    return Seat(
      type: map['name'],
      price: map['price'],
      number: map['count'],

    );
  }

  Map<String, dynamic> toJson() => {
        'price': price,
        'count': number,
        'name': type,
      };
}
