import 'dart:convert';

import 'package:tonprovider/model/seat.dart';

class Event {
  String id;
  String name;
  String image;
  String address;
  String currency;
  String description;
  int numberOfSeats;
  int numberOfAvailableSeats;
  double priceForOneSeat;
  String starName;
  String category;
  double lat;
  double lng;
  DateTime date;
  List<Seat> seats;

  Event(
      {this.id,
      this.name,
      this.image,
      this.numberOfSeats,
      this.priceForOneSeat,
      this.starName,
      this.category,
      this.currency,
      this.seats,
      this.address,
      this.lng,
      this.lat,
      this.date,
      this.numberOfAvailableSeats,
      this.description});

  factory Event.fromJson(Map<String, dynamic> map) {
    List<Seat> _seats = [];
    if (map["seatsTypes"] != null)
      _seats.addAll((map['seatsTypes'] as List)
          .map<Seat>((json) => Seat.fromJson(json))
          .toList());
    return Event(
        id: map['_id'] ?? "",
        name: map['name'] ?? " ",
        description: map['description'] ?? " ",
        image: map['imageUrl'] ?? " ",
        numberOfSeats: map['numberOfSeats'] ?? 0,
        numberOfAvailableSeats: map['numberOfAvailableSeats'] ?? 0,
//        priceForOneSeat: double.parse(map['priceForOneSeat'].toString()) ?? 0.0,
        starName: map['starName'] ?? "",
        address: map['address'] ?? "",
        category: map['category'] ?? "",
        currency: map['currency'] ?? "",
        lng: double.parse(map['lng'].toString()) ?? 0.0,
        lat: double.parse(map['lat'].toString()) ?? 0.0,
        date: DateTime.parse(map['date']),
        seats: _seats);
  }

  Map<String, dynamic> toJson() => {
        'name': name,
        'description': description ?? "test",
        'address': address ?? "Test",
        'starName': starName,
        'category': category,
        "numberOfAvailableSeats": getNumberOfAvailableSeats(),
        "priceForOneSeat": "50",
        "numberOfSeats":  getNumberOfAvailableSeats(),
        'currency': currency ?? "SAR",
        'lat': lat,
        'lng': lng,
        "date": date.toString(),
        "seatsTypes": seats
      };

  Map<String, dynamic> getSeats() {
    Map<String, dynamic> t = {};
    seats.forEach((e) {
      t[e.type] = {"price": e.price, "count": e.number};
    });
    return t;
  }

  int getNumberOfAvailableSeats() {
    int res = 0;
    seats.forEach((e) {
      res += e.number;
    });
    return res;
  }
}
