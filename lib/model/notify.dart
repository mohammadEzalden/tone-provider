class Notify {
  String message;
  DateTime date;

  Notify({
    this.message,
    this.date,
  });

  factory Notify.fromJson(Map<String, dynamic> map) {
    return Notify(
      message: map['message'] ?? "",
      date: DateTime.parse(map['date']),
    );
  }
}
