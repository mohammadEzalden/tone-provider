import 'package:tonprovider/model/role.dart';

class Employee {
  String id;
  String providerId;
  String name;
  String email;
  String phone;
  String type;
  DateTime date;
  Role role;

  Employee(
      {this.id,
      this.name,
      this.date,
      this.phone,
      this.role,
      this.email,
      this.type,
      this.providerId});

  factory Employee.fromJson(Map<String, dynamic> map) {

    return Employee(
        id: map['_id'] ,
        name: map['name'] ?? " ",
        email: map['email'] ?? " ",
        phone: map['phone'] ?? " ",
        type: map['type'] ?? " ",
        providerId: map['provider_id'] ?? " ",
        date: map['date']==null?DateTime.now():DateTime.parse(map['date']),
        role: Role.fromJson(map['roles']));
  }


  Map<String, dynamic> toJson() =>
      {
        'name': name,
        'email': email,
        'roles': role,
      };
}
