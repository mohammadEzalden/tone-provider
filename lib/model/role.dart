class Role {
  bool edit_event;
  bool add_event;
  bool QrcodeScan;
  bool edit_provider_info;
  bool employers_control;

  Role(
      {this.add_event,
      this.edit_event,
      this.edit_provider_info,
      this.employers_control,
      this.QrcodeScan});

  factory Role.fromJson(Map<String, dynamic> map) {
    return Role(
      add_event: map['add_event'] ?? false,
      edit_event: map['edit_event'] ?? false,
      edit_provider_info: map['edit_provider_info'] ?? false,
      QrcodeScan: map['QrCodeScan'] ?? false,
      employers_control: map['employers_control'] ?? false,
    );
  }

  Map<String, dynamic> toJson() =>
      {
        'add_event': add_event,
        'edit_event': edit_event,
        'edit_provider_info': edit_provider_info,
        'QrCodeScan': QrcodeScan,
        'employers_control': employers_control,
      };


}
