import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:tonprovider/model/employee.dart';
import 'package:tonprovider/apis/api/api.dart' as api;
import 'package:tonprovider/model/role.dart';

class EmployeeViewModel extends ChangeNotifier {
  List<Employee> employees = [];
  bool employeesLoading = true;

  EmployeeViewModel() {
    getEmployees();
  }

  getEmployees() async {
    employees = await api.getEmployees();
    employeesLoading = false;
    notifyListeners();
  }

  addEmployee({String email, String password, BuildContext context}) async {
    Employee e = await api.addEmployees(
        email: email, password: password, context: context);
    if (e != null) {
      employees.add(e);
      Navigator.pop(context);
    }
    notifyListeners();
  }

  editEmployee({Employee e ,BuildContext context}) async {
    Employee temp = await api.editEmployee(
        e: e, context: context);
    if (temp != null) {
      int i=employees.indexOf(e);
      employees.remove(e);
      employees.insert(i, temp);
      Navigator.pop(context);
    }
    notifyListeners();
  }

  deleteEmployee(String id,BuildContext context)async{
   bool res =await api.deleteEmployee(context: context,id: id);
   if(res){
     employees.removeWhere((element) => element.id==id);
     Navigator.pop(context);
     notifyListeners();
   }
  }
}
