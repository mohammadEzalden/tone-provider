import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tonprovider/model/event.dart';
import 'package:tonprovider/apis/auth/api_handler.dart' as http;
import 'package:tonprovider/apis/auth/url.dart' as url;
import 'package:tonprovider/apis/api/api.dart' as api;
import 'package:tonprovider/model/notify.dart';

class HomeViewModel extends ChangeNotifier {
  List<Event> events = [];
  List<String> categories = [];
  List<Event> eventsByCategory = [];
  List<Notify> notifies = [];
  List<String> seats = [];
  bool eventsLoading = true;

  HomeViewModel() {
    getNotifies();
//    getSeats();
    getCategories();
  }

  getEvents() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await http.getApi(
        url: url.getEvent(prefs.getString("providerID")),
        result: (int code, dynamic data) {
          events=[];
          for (var item in data) {
            events.add(Event.fromJson(item));
          }
          eventsLoading = false;
          events = events.reversed.toList();

          notifyListeners();
        });
  }

  getSeats() async {
    await http.getApi(
        url:url.seats(),
        result: (int code, dynamic data) {
          seats=[];
          for (var item in data) {
            seats.add(item);
          }
          notifyListeners();
        });
  }

  getNotifies() async {
    getEvents();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await http.getApi(
        url: url.notification(prefs.getString("providerID")),
        result: (int code, dynamic data) {
          notifies=[];
          for (var item in data) {
            notifies.add(Notify.fromJson(item));
          }
          notifies = notifies.reversed.toList();

          notifyListeners();
        });
    api.readNotification();
  }

  getCategories() async {
    http.getApi(
        url: url.getCategories(),
        result: (int code, dynamic data) {
          for (var item in data['categories']) {
            categories.add(item);
          }
          notifyListeners();
        });
  }

  addEvent(Event event, BuildContext context, File image) async {
    Event res =
        await api.addEvent(context: context, image: image, event: event);
    if (res != null) {
      Navigator.pop(context);
//      events.add(res);
    } else
      Navigator.pop(context);
  }

  editEvent(Event event, BuildContext context, File image) async {
    Event res =
        await api.editEvent(context: context, image: image, event: event);
    if (res != null) {
      Navigator.pop(context);
      Navigator.pop(context);

      int i=events.indexWhere((element) => element.id==event.id);
      events.removeAt(i);
      events.insert(i, res);
      notifyListeners();
    } else
      Navigator.pop(context);
  }

  deleteEvent({String id, BuildContext context}) async {
    bool res =
        await api.deleteEvent(context: context, id: id);
    if (res) {
      Navigator.pop(context);
      Navigator.pop(context);
      Navigator.pop(context);
      events.removeWhere((element) => element.id==id);
      notifyListeners();
    } else
      Navigator.pop(context);
  }

  filterBySearch(String query) {
    eventsByCategory = [];
    eventsByCategory.addAll(events);
    print(query);
    if (query != "") {
      eventsByCategory.removeWhere((element) => !element.name.contains(query));
    }

    notifyListeners();
  }

}
