import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tonprovider/const/widgets.dart';
import 'package:tonprovider/language/appLocalizations.dart';
import 'package:tonprovider/model/employee.dart';
import 'package:tonprovider/model/profile.dart';
import 'package:tonprovider/model/user.dart';
import 'package:tonprovider/apis/api/api.dart' as api;

class UserViewModel extends ChangeNotifier {
  bool loadingData;
  bool isLogin;
  String email;
  String name;
  String phone;
  Employee employee;
  bool isProvider;
  User user;

  UserViewModel() {
    isLogin = true;
    isProvider=true;
    updateProfileInfo();
  }

  profileInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    isProvider = prefs.getBool("isProvider");
    final _user = jsonDecode(prefs.getString("user"));
    user = User.fromJson(jsonDecode(prefs.getString("user")));
    name = _user['name'];
    email = _user['email'];
    if (!isProvider) {
      employee = Employee.fromJson(_user);
      print(employee);
    }
    isLogin = false;
    notifyListeners();
  }

  updateProfileInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await api.getProfileInfo();
    user = User.fromJson(jsonDecode(prefs.getString("user")));
    final _user = jsonDecode(prefs.getString("user"));
    isProvider = prefs.getBool("isProvider");
    if(!isProvider)
    employee = Employee.fromJson(_user);
    profileInfo();
    notifyListeners();
  }

  editUser(User u, {File image, BuildContext context}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    u.id = prefs.getString("providerID");
    User res = await api.editUser(u: u, image: image);
    if (res != null) {
      user = res;
      prefs.setString("user", jsonEncode(res));
      showToast(AppLocalizations.of(context).translate("Success"), context);
      Navigator.pop(context);
      Navigator.pop(context);
      notifyListeners();
    }
  }
}
