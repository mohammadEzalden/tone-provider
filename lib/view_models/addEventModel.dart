import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tonprovider/model/event.dart';
import 'package:tonprovider/apis/auth/api_handler.dart' as http;
import 'package:tonprovider/apis/auth/url.dart' as url;
import 'package:tonprovider/apis/api/api.dart' as api;
import 'package:tonprovider/model/notify.dart';

class AddEventViewModel extends ChangeNotifier {
  List<String> seats = [];
  List<String> temp = [];
  List<Widget>numberOfSeatType;

  AddEventViewModel() {
    getSeats();
  }

  getSeats() async {
    await http.getApi(
        url: url.seats(),
        result: (int code, dynamic data) {
          seats = [];
          for (var item in data['seats']) {
            seats.add(item);
          }
          temp=[];
          temp.addAll(seats);
          notifyListeners();
        });

  }

  addSeatType(String type){
    temp.add(type);
    notifyListeners();
  }

  removeType(String type){
    temp.remove(type);
    notifyListeners();
  }

  reset(){
    temp=[];
    temp.addAll(seats);

    notifyListeners();
  }
}
