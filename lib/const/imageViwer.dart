import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:tonprovider/const/myColors.dart';

class ImageViewer extends StatelessWidget {
  final String url;

  ImageViewer(this.url);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
          children: [
            PhotoView(
              imageProvider: NetworkImage(url),
              loadingBuilder: (context, progress) => Center(
                child: Container(
                  width: 25.0,
                  height: 25.0,
                  child: CircularProgressIndicator(),
                ),
              ),
              minScale: PhotoViewComputedScale.contained,
              maxScale: PhotoViewComputedScale.covered * 1.8,
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Align(
                alignment: Alignment.topRight,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 60,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        FloatingActionButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          materialTapTargetSize: MaterialTapTargetSize.padded,
                          backgroundColor: myColors.purple100,
                          child: const Icon(Icons.arrow_back, size: 36.0),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}
