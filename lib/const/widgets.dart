import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:url_launcher/url_launcher.dart';

void showToast(String message, BuildContext context,
    {ToastGravity gravity = ToastGravity.BOTTOM}) {
  FToast fToast;
  fToast = FToast();
  fToast.init(context);
  Widget toast = Container(
    padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(25.0),
      border: Border.all(color: Color(0xFF904E95), width: 2),
      color: Colors.white,
    ),
    child: Text(
      message,
      style: TextStyle(color: Color(0xFF904E95)),
    ),
  );

  fToast.showToast(
    child: toast,
    gravity: gravity,
    toastDuration: Duration(seconds: 2),
  );
}

buildShowDialog(BuildContext context) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Center(
          child: CircularProgressIndicator(),
        );
      });
}


launchURL(String url) async {
  String _uri = Uri.encodeFull(url);
  if (await canLaunch(_uri)) {
    await launch(_uri);
  } else {
    throw 'Could not launch $url';
  }
}


