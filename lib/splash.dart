import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tonprovider/const/myColors.dart';
import 'package:tonprovider/apis/api/api.dart';
import 'package:tonprovider/apis/auth/url.dart' as url;
import 'package:http/http.dart' as http;

import 'language/appLanguage.dart';

class Splash extends StatefulWidget {
  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<Splash> {
  startTime() async {
    var _duration = Duration(milliseconds: 500);
    return Timer(_duration, navigationPage);
  }

  Future<bool> isActivate() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, String> header = {'Content-Type': 'application/json'};
    var response;
    print(prefs.getString("loginInfo"));
    Uri uri = Uri.parse(url.login());
    response = await http.post(uri,
        body: jsonEncode(jsonDecode(prefs.getString("loginInfo"))),
        headers: header);
    print(response.body);
    final parsed = await jsonDecode(response.body);
    print(parsed);
    if (response.statusCode != HttpStatus.ok) {
      prefs.clear();
      return false;
    }

    if (parsed['user'] == null || parsed['user']['active'] == false) {
      prefs.clear();
      return false;
    }
    prefs.setString("user", jsonEncode(parsed['user']));
    prefs.setString("token", parsed['token']);
    return true;
  }

  void navigationPage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString("language") == null) {
      prefs.setString("language", "en");
    }
    Provider.of<AppLanguage>(context, listen: false)
        .changeLanguage(Locale(prefs.getString("language")));
    if (prefs.getBool("isLogin") == null) {
      print('User is currently signed out!');
      Navigator.pushReplacementNamed(context, '/LoginPage');
    } else {
      if (!(await isActivate())) {
        Navigator.pushReplacementNamed(context, '/LoginPage');
      } else if (prefs.getBool("isProvider"))
        Navigator.pushReplacementNamed(context, '/HomePage');
      else
        Navigator.pushReplacementNamed(context, '/HomeEmployeePage');
    }
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: myColors.purple100,
        body: Container(
          decoration: BoxDecoration(color: Colors.white),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  "assets/images/logo.png",
                  width: width / 3,
                ),
                SizedBox(
                  height: 10,
                ),
                Image.asset(
                  "assets/images/tone.png",
                  width: width / 3,
                ),
              ],
            ),
          ),
        ));
  }
}
