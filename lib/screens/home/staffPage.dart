import 'dart:io';

import 'package:email_validator/email_validator.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:tonprovider/const/myColors.dart';
import 'package:tonprovider/language/appLocalizations.dart';
import 'package:tonprovider/model/employee.dart';
import 'package:tonprovider/model/role.dart';
import 'package:tonprovider/view_models/emolyeeViewModel.dart';
import 'package:tonprovider/view_models/home_view_model.dart';
import 'package:tonprovider/view_models/user_view_model.dart';

class StaffPage extends StatefulWidget {
  @override
  _StaffPageState createState() => _StaffPageState();
}

class _StaffPageState extends State<StaffPage> {
  final TextEditingController email = TextEditingController();
  final TextEditingController password = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool employers_control = false;
  bool edit_provider_info = false;
  bool QrcodeScan = false;
  bool add_event = false;
  bool edit_event = false;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();
  @override
  void initState() {
    super.initState();
    if (Platform.isIOS) iOS_Permission();
    const AndroidInitializationSettings initializationSettingsAndroid =
    AndroidInitializationSettings('app_icon');
    final IOSInitializationSettings initializationSettingsIOS =
    IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    final MacOSInitializationSettings initializationSettingsMacOS =
    MacOSInitializationSettings();
    final InitializationSettings initializationSettings =
    InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS,
        macOS: initializationSettingsMacOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print(message);


        const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails('your channel id', 'your channel name',
            'your channel description',
            importance: Importance.max,
            priority: Priority.high,
            showWhen: false);
        const NotificationDetails platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);
        await flutterLocalNotificationsPlugin.show(
            0,
            message['notification']['title'],
            message['notification']['body'],
            platformChannelSpecifics,
            payload: 'shopper');
        Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
        Provider.of<HomeViewModel>(context, listen: false).getNotifies();
      },
      onLaunch: (Map<String, dynamic> message) async {
        print(message);
        Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();

        Provider.of<HomeViewModel>(context, listen: false).getNotifies();
      },
      onResume: (Map<String, dynamic> message) async {
        Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
        Provider.of<HomeViewModel>(context, listen: false).getNotifies();

        print(message);
      },
    );

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  Future selectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: $payload');
    }
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
    Provider.of<HomeViewModel>(context, listen: false).getNotifies();
  }

  Future<dynamic> onSelectNotification(String payload) async {
    Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
    Provider.of<HomeViewModel>(context, listen: false).getNotifies();
  }
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0), // here the desired height
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.topRight,
                    colors: [
                      myColors.orange,
                      myColors.purple100,
                    ]),
              )),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 8, right: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: height/29,
              ),
              SvgPicture.asset("assets/icons/tone.svg",color: myColors.purple100,),
              SizedBox(                height: height/50,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      width: width/2.5,
                      child: Divider()),
                ],
              ),
              SizedBox(
                height: height/50,

              ),
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Row(
                  children: [
                    Consumer<UserViewModel>(builder: (context,user,child){
                      if(user.isProvider)return Container();
                      return Icon(
                        Icons.arrow_back,
                        color: myColors.purple100,
                      );
                    },),
                    Text(
                      AppLocalizations.of(context).translate("Employee"),
                      style: TextStyle(
                          fontSize: 18,
                          color: myColors.purple100,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),

              SizedBox(
                height: height/29,

              ),
              Consumer<EmployeeViewModel>(builder: (context, employee, child) {
                if (employee.employeesLoading)
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                if (employee.employees.isEmpty)
                  return Padding(
                    padding: EdgeInsets.only(top: height / 4),
                    child: Center(child: Text(AppLocalizations.of(context).translate("You Don't have any employee"),                        style: TextStyle(color: Colors.grey, fontSize: 20),
                    )),
                  );


                return Column(
                  children: employee.employees
                      .map((e) => Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Text(
                          e.email,
                          style: TextStyle(
                              color: myColors.purple100, fontSize: 18),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                              icon:  SvgPicture.asset(
                                "assets/svg_icons/Icon material-edit.svg",
                              ),
                              onPressed: () {
                                editEmployee(context, width, e);
                              }),

                          e.role.employers_control?SvgPicture.asset(
                            "assets/svg_icons/iconfinder_m-49_4230521.svg",
                          ):  SvgPicture.asset(
                            "assets/svg_icons/Vector.svg",
                          ),
                          IconButton(
                              icon: SvgPicture.asset("assets/svg_icons/Icon material-delete.svg",color: Colors.red,),
                              onPressed: () {
                                deleteEmployee(context,width,e);
                              }),
                        ],
                      )
                    ],
                  ))
                      .toList(),
                );
              }),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: Padding(
        padding:  EdgeInsets.only(bottom: 10),
        child: TextButton(
            style: ButtonStyle(
                padding: MaterialStateProperty.all(
                    EdgeInsets.symmetric(horizontal: width/5, vertical: height/55)),
                backgroundColor:
                MaterialStateProperty.all(myColors.purple100),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.grey[300])))),
            onPressed: () async {
              addEmployee(context, width);
            },
            child: Text(
              AppLocalizations.of(context).translate("Add Employee"),
              style: TextStyle(color: myColors.white),
            )),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,

    );
  }

  buildShowDialog(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  addEmployee(BuildContext context, double width) {
    showDialog(
        context: context,
        barrierDismissible: false,
        barrierColor: Color(0xBFFFFFFF),
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              AppLocalizations.of(context).translate("Add Employee"),
              style: TextStyle(
                  color: myColors.purple100, fontWeight: FontWeight.bold),
            ),
            content: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return Container(
                  child: SingleChildScrollView(
                    child: Form(
                      key: _formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            AppLocalizations.of(context).translate("Email Employee"),
                            style: TextStyle(color: myColors.purple100),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            width: width / 1.3,
                            padding: EdgeInsets.only(left: 20, right: 20),
                            child: TextFormField(
                              validator: (v) {
                                if (v != null && v.length == 0) {
                                  return AppLocalizations.of(context).translate("Please Enter Email");
                                }
                                if (!EmailValidator.validate(email.text)) {
                                  return AppLocalizations.of(context)
                                      .translate("Email isn't valid");
                                }
                                return null;
                              },
                              textInputAction: TextInputAction.done,
                              decoration: InputDecoration(
                                  hintText: 'tone@tone.com',
                                  border: InputBorder.none,
                                  hintStyle:
                                  TextStyle(color: myColors.purple69)),
                              controller: email,
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                                border: Border.all(color: myColors.purple100)),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Text(
                            AppLocalizations.of(context).translate("Password"),
                            style: TextStyle(color: myColors.purple100),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            width: width / 1.3,
                            padding: EdgeInsets.only(left: 20, right: 20),
                            child: TextFormField(
                              validator: (v) {
                                if (v != null && v.length == 0) {
                                  return AppLocalizations.of(context).translate("Please Enter Password");
                                }
                                return null;
                              },
                              obscureText: true,
                              textInputAction: TextInputAction.done,
                              decoration: InputDecoration(
                                  hintText: '*********',
                                  border: InputBorder.none,
                                  hintStyle:
                                  TextStyle(color: myColors.purple69)),
                              controller: password,
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                                border: Border.all(color: myColors.purple100)),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              InkWell(
                                onTap: () {
                                  email.text = "";
                                  password.text = "";
                                  Navigator.pop(context);
                                },
                                child: Container(
                                  width: 90,
                                  padding: EdgeInsets.only(
                                      left: 8, right: 8, top: 12, bottom: 12),
                                  child: Center(
                                    child: Text(
                                      AppLocalizations.of(context).translate("Discard"),
                                      style: TextStyle(
                                          color: myColors.purple100,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(30),
                                      border: Border.all(
                                          color: myColors.purple100)),
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              RawMaterialButton(
                                fillColor: myColors.purple100,
                                splashColor: myColors.purple100,
                                shape: StadiumBorder(
                                    side:
                                    BorderSide(color: myColors.purple100)),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 30.0, vertical: 15.0),
                                  child: Text(
                                    AppLocalizations.of(context).translate("Save"),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                onPressed: () {
                                  if (_formKey.currentState.validate()) {
                                    buildShowDialog(context);
                                    Provider.of<EmployeeViewModel>(context,
                                        listen: false)
                                        .addEmployee(
                                        password: password.text.trim(),
                                        email: email.text.trim(),
                                        context: context);
                                  }
                                },
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 30,
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0)), //this right here
          );
        });
  }

  editEmployee(BuildContext context, double width, Employee e) {
    employers_control = e.role.employers_control;
    edit_provider_info = e.role.edit_provider_info;
    QrcodeScan = e.role.QrcodeScan;
    add_event = e.role.add_event;
    edit_event = e.role.edit_event;
    showDialog(
        context: context,
        builder: (BuildContext cxt) {
          return AlertDialog(
            title: Text(
              e.email,
              style: TextStyle(
                  color: myColors.purple100, fontWeight: FontWeight.bold),
            ),
            content: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return Container(
                  width: width,
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        ListTile(
                          title: Text(AppLocalizations.of(context).translate("Employers Control")),
                          trailing: Switch(
                            value: employers_control,
                            onChanged: (v) {
                              setState(() {
                                employers_control = v;
                              });
                            },
                          ),
                        ),
                        ListTile(
                          title: Text(AppLocalizations.of(context).translate("Add Event")),
                          trailing: Switch(
                            value: add_event,
                            onChanged: (v) {
                              add_event = v;
                              setState(() { });

                            },
                          ),
                        ),
                        ListTile(
                          title: Text(AppLocalizations.of(context).translate("QR Code Scan")),
                          trailing: Switch(
                            value: QrcodeScan,
                            onChanged: (v) {
                              QrcodeScan = v;
                              setState(() { });
                            },
                          ),
                        ),
//                        ListTile(
//                          title: Text(AppLocalizations.of(context).translate("Edit Provider Info")),
//                          trailing: Switch(
//                            value: edit_provider_info,
//                            onChanged: (v) {
//                              edit_provider_info = v;
//                              setState(() { });
//                            },
//                          ),
//                        ),
                        ListTile(
                          title: Text(AppLocalizations.of(context).translate("Edit Event")),
                          trailing: Switch(
                            value: edit_event,
                            onChanged: (v) {
                              edit_event = v;
                              setState(() { });
                            },
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            InkWell(
                              onTap: () {
                                employers_control = e.role.employers_control;
                                edit_provider_info = e.role.edit_provider_info;
                                QrcodeScan = e.role.QrcodeScan;
                                add_event = e.role.add_event;
                                edit_event = e.role.edit_event;
                                Navigator.pop(context);
                              },
                              child: Container(
                                width: 90,
                                padding: EdgeInsets.only(
                                    left: 8, right: 8, top: 12, bottom: 12),
                                child: Center(
                                  child: Text(
                                    AppLocalizations.of(context).translate("Discard"),
                                    style: TextStyle(
                                        color: myColors.purple100,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    border:
                                    Border.all(color: myColors.purple100)),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            RawMaterialButton(
                              fillColor: myColors.purple100,
                              splashColor: myColors.purple100,
                              shape: StadiumBorder(
                                  side: BorderSide(color: myColors.purple100)),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 30.0, vertical: 15.0),
                                child: Text(
                                  AppLocalizations.of(context).translate("Save"),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              onPressed: () {
                                buildShowDialog(context);
                                e.role = Role(
                                    add_event: add_event,
                                    edit_event: edit_event,
                                    edit_provider_info: edit_provider_info,
                                    employers_control: employers_control,
                                    QrcodeScan: QrcodeScan);
                                Provider.of<EmployeeViewModel>(context,
                                    listen: false)
                                    .editEmployee(e: e, context: context);
                              },
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0)), //this right here
          );
        });
  }

  deleteEmployee(BuildContext context, double width, Employee e) {
    showDialog(
        context: context,
        builder: (BuildContext cxt) {
          return AlertDialog(
            title: Text(
              AppLocalizations.of(context).translate("Alert"),
              style: TextStyle(
                  color: myColors.purple100, fontWeight: FontWeight.bold),
            ),
            content: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return Container(
                  width: width,
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(AppLocalizations.of(context).translate("Are you sure you want to delete Employee?"),style: TextStyle(color: myColors.purple100),),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Container(
                                width: width/4,

                                padding: EdgeInsets.only(
                                    left: 8, right: 8, top: 12, bottom: 12),
                                child: Center(
                                  child: Text(
                                    AppLocalizations.of(context).translate("Discard"),
                                    style: TextStyle(
                                        color: myColors.purple100,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    border:
                                    Border.all(color: myColors.purple100)),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            RawMaterialButton(
                              fillColor: myColors.purple100,
                              splashColor: myColors.purple100,
                              shape: StadiumBorder(
                                  side: BorderSide(color: myColors.purple100)),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: width/13, vertical: 15.0),
                                child: Text(
                                  AppLocalizations.of(context).translate("Delete"),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              onPressed: () {
                                buildShowDialog(context);
                                Provider.of<EmployeeViewModel>(context,
                                    listen: false)
                                    .deleteEmployee(e.id,context);

                              },
                            ),
                          ],
                        ),

                      ],
                    ),
                  ),
                );
              },
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0)), //this right here
          );
        });
  }
}
