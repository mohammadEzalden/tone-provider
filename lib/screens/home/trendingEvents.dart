import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tonprovider/const/myColors.dart';
import 'package:tonprovider/customWidget/image_from_network.dart';

class TrendingEvents extends StatefulWidget {
  @override
  _TrendingEventsState createState() => _TrendingEventsState();
}

class _TrendingEventsState extends State<TrendingEvents> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
      height: 210,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          SizedBox(width: width/11,),
          InkWell(
            onTap: (){
              Navigator.pushNamed(context, "/EventDetailsPage");
            },
            child: Container(
              height: 210,
              width: 207,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: myColors.purple100,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  Container(
                    height: 140,
                    width: 207,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: ImageFromNetworkWidget(
                          "https://images.unsplash.com/photo-1517263904808-5dc91e3e7044?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80"),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20,top: 8),
                    child: Text("Sunday from 11/11/2022\n to 12/12/22\n at 00:00 am",style: TextStyle(color: myColors.white,),),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(width: 35,),
          InkWell(
            onTap: (){
              Navigator.pushNamed(context, "/EventDetailsPage");
            },
            child: Container(
              height: 210,
              width: 207,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: myColors.orange,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 140,
                    width: 207,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: ImageFromNetworkWidget(
                          "https://images.unsplash.com/photo-1475721027785-f74eccf877e2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=750&q=80"),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20,top: 8),
                    child: Text("Sunday from 11/11/2022\n to 12/12/22\n at 00:00 am",style: TextStyle(color: myColors.white),),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(width: 35,),

        ],
      ),
    );
  }
}
