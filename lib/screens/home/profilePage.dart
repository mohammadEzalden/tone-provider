import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tonprovider/const/myColors.dart';
import 'package:tonprovider/const/widgets.dart';
import 'package:tonprovider/customWidget/image_from_network.dart';
import 'package:tonprovider/language/appLocalizations.dart';
import 'package:tonprovider/view_models/home_view_model.dart';
import 'package:tonprovider/view_models/user_view_model.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final TextEditingController code = TextEditingController();
  String category = "all";

  @override
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();
  @override
  void initState() {
    super.initState();
    if (Platform.isIOS) iOS_Permission();
    const AndroidInitializationSettings initializationSettingsAndroid =
    AndroidInitializationSettings('app_icon');
    final IOSInitializationSettings initializationSettingsIOS =
    IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    final MacOSInitializationSettings initializationSettingsMacOS =
    MacOSInitializationSettings();
    final InitializationSettings initializationSettings =
    InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS,
        macOS: initializationSettingsMacOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print(message);


        const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails('your channel id', 'your channel name',
            'your channel description',
            importance: Importance.max,
            priority: Priority.high,
            showWhen: false);
        const NotificationDetails platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);
        await flutterLocalNotificationsPlugin.show(
            0,
            message['notification']['title'],
            message['notification']['body'],
            platformChannelSpecifics,
            payload: 'shopper');
        Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
        Provider.of<HomeViewModel>(context, listen: false).getNotifies();
      },
      onLaunch: (Map<String, dynamic> message) async {
        print(message);
        Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();

        Provider.of<HomeViewModel>(context, listen: false).getNotifies();
      },
      onResume: (Map<String, dynamic> message) async {
        Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
        Provider.of<HomeViewModel>(context, listen: false).getNotifies();

        print(message);
      },
    );

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  Future selectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: $payload');
    }
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
    Provider.of<HomeViewModel>(context, listen: false).getNotifies();
  }

  Future<dynamic> onSelectNotification(String payload) async {
    Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
    Provider.of<HomeViewModel>(context, listen: false).getNotifies();
  }
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0), // here the desired height
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.topRight,
                    colors: [
                      myColors.orange,
                      myColors.purple100,
                    ]),
              )),
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              SizedBox(
                height: height / 29,
              ),
              Consumer<UserViewModel>(builder: (context, user, child) {
                Widget imageWidget;
                if (user.user != null && user.user.imageUrl.isNotEmpty) {
                  imageWidget = Container(
                    height: 125.0,
                    width: 125.0,
                    decoration: new BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: const Color(0x33A6A6A6)),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10000.0),
                      child: ImageFromNetworkWidget(
                        user.user.imageUrl,
                        boxFit: BoxFit.cover,
                      ),
                    ),
                  );
                } else
                  imageWidget = Container(
                    height: 125.0,
                    width: 125.0,
                    decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: const Color(0x33A6A6A6)),
                        image: DecorationImage(
                            image: AssetImage("assets/images/person.jpeg"),
                            fit: BoxFit.cover)),
                  );
                return Column(
                  children: [
                    imageWidget,
                    SizedBox(
                      height: 11,
                    ),
                    Text(
                      user.user == null ? " " : user.user.name,
                      style: TextStyle(
                          fontSize: 18,
                          color: myColors.purple100,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                );
              }),
              SizedBox(
                height: height / 20,
              ),
              InkWell(
                onTap: () {
                  Navigator.pushNamed(context, "/PersonalInfoPage");
                },
                child: Row(
                  children: [
                    SizedBox(
                      width: 12,
                    ),
                    SvgPicture.asset(
                      "assets/icons/account-circle.svg",
                    ),
                    Padding(
                      padding:
                      EdgeInsets.only(left: width / 17, right: width / 17),
                      child: Text(
                        AppLocalizations.of(context).translate("Personal Info"),
                        style:
                        TextStyle(fontSize: 18, color: myColors.purple100),
                      ),
                    ),
                    Expanded(child: Container()),
                    Icon(
                      Icons.arrow_forward_ios,
                      color: myColors.purple100,
                      size: 17,

                    ),
                    SizedBox(
                      width: 12,
                    )
                  ],
                ),
              ),
              SizedBox(
                height: height / 30,
              ),
              InkWell(
                onTap: () {
                  Navigator.pushNamed(context, "/SettingsPage");
                },
                child: Row(
                  children: [
                    SizedBox(
                      width: 12,
                    ),
                    SvgPicture.asset(
                      "assets/icons/Icon ionic-ios-settings.svg",
                    ),
                    Padding(
                      padding:
                      EdgeInsets.only(left: width / 17, right: width / 17),
                      child: Text(
                        AppLocalizations.of(context).translate("Settings"),
                        style:
                        TextStyle(fontSize: 18, color: myColors.purple100),
                      ),
                    ),
                    Expanded(child: Container()),
                    Icon(
                      Icons.arrow_forward_ios,
                      color: myColors.purple100,
                      size: 17,
                    ),
                    SizedBox(
                      width: 12,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: height / 30,
              ),
              InkWell(
                onTap: (){
                  launchURL("https://toneap.com/");
                },
                child: Row(
                  children: [
                    SizedBox(
                      width: 12,
                    ),
                    SvgPicture.asset(
                      "assets/icons/Icon feather-info.svg",
                    ),
                    Padding(
                      padding:
                      EdgeInsets.only(left: width / 17, right: width / 17),
                      child: Text(
                        AppLocalizations.of(context).translate("About US"),
                        style:
                        TextStyle(fontSize: 18, color: myColors.purple100),
                      ),
                    ),
                    Expanded(child: Container()),
                    Icon(
                      Icons.arrow_forward_ios,
                      size: 17,

                      color: myColors.purple100,
                    ),
                    SizedBox(
                      width: 12,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: height / 30,
              ),
              InkWell(
                onTap: () async {
                  showDialog(
                      context: context,
                      builder: (BuildContext cxt) {
                        return AlertDialog(
                          title: Text(
                            AppLocalizations.of(context).translate("Alert"),
                            style: TextStyle(
                                color: myColors.purple100,
                                fontWeight: FontWeight.bold),
                          ),
                          content: StatefulBuilder(
                            builder:
                                (BuildContext context, StateSetter setState) {
                              return Container(
                                child: SingleChildScrollView(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        AppLocalizations.of(context).translate(
                                            "Are you sure you want to Log out?"),
                                        style: TextStyle(
                                            color: myColors.purple100),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.end,
                                        children: [
                                          InkWell(
                                            onTap: () {
                                              Navigator.pop(context);
                                            },
                                            child: Container(
                                              width: width / 4,
                                              padding: EdgeInsets.only(
                                                  left: 8,
                                                  right: 8,
                                                  top: 12,
                                                  bottom: 12),
                                              child: Center(
                                                child: Text(
                                                  AppLocalizations.of(context)
                                                      .translate("Discard"),
                                                  style: TextStyle(
                                                      color: myColors.purple100,
                                                      fontWeight:
                                                      FontWeight.bold),
                                                ),
                                              ),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                  BorderRadius.circular(30),
                                                  border: Border.all(
                                                      color:
                                                      myColors.purple100)),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          RawMaterialButton(
                                            fillColor: myColors.purple100,
                                            splashColor: myColors.purple100,
                                            shape: StadiumBorder(
                                                side: BorderSide(
                                                    color: myColors.purple100)),
                                            child: Padding(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: width / 19,
                                                  vertical: 15.0),
                                              child: Text(
                                                AppLocalizations.of(context)
                                                    .translate("Log out"),
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight:
                                                    FontWeight.bold),
                                              ),
                                            ),
                                            onPressed: () async {
                                              SharedPreferences prefs = await SharedPreferences.getInstance();
                                              prefs.clear();
//                                              Navigator.pop(context);
                                              Navigator.pop(context);
                                              Navigator.pushReplacementNamed(context, '/LoginPage');
                                            },
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(
                                  15.0)), //this right here
                        );
                      });
                },
                child: Row(
                  children: [
                    SizedBox(
                      width: 12,
                    ),
                    Icon(
                      Icons.logout,
                      color: myColors.purple100,
                    ),
                    Padding(
                      padding:
                      EdgeInsets.only(left: width / 17, right: width / 17),
                      child: Text(
                        AppLocalizations.of(context).translate("Log out"),
                        style:
                        TextStyle(fontSize: 18, color: myColors.purple100),
                      ),
                    ),
                    Expanded(child: Container()),
                    Icon(
                      Icons.arrow_forward_ios,
                      size: 17,

                      color: myColors.purple100,
                    ),
                    SizedBox(
                      width: 12,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }


}
