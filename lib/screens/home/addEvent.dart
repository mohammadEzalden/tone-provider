import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tonprovider/const/myColors.dart';
import 'package:tonprovider/const/widgets.dart';
import 'package:tonprovider/apis/api/api.dart' as api;
import 'package:tonprovider/customWidget/seatPage.dart';
import 'package:tonprovider/language/appLocalizations.dart';
import 'package:tonprovider/model/event.dart';
import 'package:tonprovider/model/seat.dart';
import 'package:tonprovider/view_models/addEventModel.dart';
import 'package:tonprovider/view_models/home_view_model.dart';

class AddEventPage extends StatefulWidget {
  @override
  AddEventPageState createState() => AddEventPageState();
}

class AddEventPageState extends State<AddEventPage> {
  final TextEditingController eventName = TextEditingController();
  final TextEditingController starName = TextEditingController();
  final TextEditingController dateCont = TextEditingController();
  final TextEditingController numberOfSeats = TextEditingController();
  final TextEditingController priceForOneSeat = TextEditingController();
  final TextEditingController description = TextEditingController();
  GoogleMapController _controller;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final f = new DateFormat('yyyy-MM-dd HH:mm');
  int count = 1;
  String category;
  LocationResult result;
  static List<Seat> seats = [Seat()];

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0), // here the desired height
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
              decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.topRight,
                colors: [
                  myColors.orange,
                  myColors.purple100,
                ]),
          )),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(left: 8, right: 8),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: height / 29,
                ),
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Row(
                    children: [
                      Icon(
                        Icons.arrow_back,
                        color: myColors.purple100,
                      ),
                      Text(
                        AppLocalizations.of(context).translate("Add Event"),
                        style: TextStyle(
                            fontSize: 18,
                            color: myColors.purple100,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: height / 33,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 12, right: 12),
                  child: Row(
                    children: [
                      Text(
                        AppLocalizations.of(context).translate("Event Name"),
                        style: TextStyle(color: myColors.purple100),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: width - 16,
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: TextFormField(
                    validator: (v) {
                      if (v != null && v.length == 0) {
                        return AppLocalizations.of(context)
                            .translate("Please Enter Event Name");
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                        errorStyle: TextStyle(height: 0.05),
                        hintText:
                            AppLocalizations.of(context).translate("text"),
                        border: InputBorder.none,
                        hintStyle: TextStyle(color: myColors.purple69)),
                    controller: eventName,
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      border: Border.all(color: myColors.purple100)),
                ),
                SizedBox(
                  height: 12,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 12, right: 12),
                  child: Row(
                    children: [
                      Text(
                        AppLocalizations.of(context).translate("Type of event"),
                        style: TextStyle(color: myColors.purple100),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 12,
                ),
                Container(
                  width: width - 16,
                  child: Consumer<HomeViewModel>(
                    builder: (context, home, child) {
                      return DropdownButtonFormField(
                        decoration: InputDecoration(
                            errorStyle: TextStyle(height: 0.05),
                            contentPadding: EdgeInsets.only(
                                left: 20, top: 7, bottom: 7, right: 24),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  width: 1.0,
                                  style: BorderStyle.solid,
                                  color: myColors.purple100),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30.0)),
                            ),
                            filled: true,
                            hintStyle: TextStyle(color: myColors.purple100),
                            hintText: AppLocalizations.of(context)
                                .translate("Type of event"),
                            fillColor: Colors.white),
                        value: category,
                        onChanged: (String Value) {
                          setState(() {
                            category = Value;
                          });
                        },
                        items: home.categories
                            .map((cityTitle) => DropdownMenuItem(
                                value: cityTitle,
                                child: Text(
                                  "$cityTitle",
                                  style: TextStyle(color: myColors.purple100),
                                )))
                            .toList(),
                      );
                    },
                  ),
                ),
                SizedBox(
                  height: 12,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 12, right: 12),
                  child: Row(
                    children: [
                      Text(
                        AppLocalizations.of(context).translate("Name of star"),
                        style: TextStyle(color: myColors.purple100),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 12,
                ),
                Container(
                  width: width - 16,
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: TextFormField(
                    validator: (v) {
                      if (v != null && v.length == 0) {
                        return AppLocalizations.of(context)
                            .translate("Please Enter Star Name");
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                        errorStyle: TextStyle(height: 0.05),
                        hintText:
                            AppLocalizations.of(context).translate("text"),
                        border: InputBorder.none,
                        hintStyle: TextStyle(color: myColors.purple69)),
                    controller: starName,
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      border: Border.all(color: myColors.purple100)),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  color: Colors.grey,
                  height: 1,
                  width: width / 1.3,
                ),
                SizedBox(
                  height: 17,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 12, right: 12),
                  child: Row(
                    children: [
                      Text(
                        AppLocalizations.of(context).translate("Date"),
                        style: TextStyle(color: myColors.purple100),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 12,
                ),
                Container(
                  width: width - 16,
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: TextFormField(
                    readOnly: true,
                    validator: (v) {
                      if (v != null && v.length == 0) {
                        return AppLocalizations.of(context)
                            .translate("Please Enter Date");
                      }
                      return null;
                    },
                    onTap: () {
                      DatePicker.showDateTimePicker(context,
                          showTitleActions: true,
                          minTime: DateTime.now(),
                          maxTime: DateTime(2101, 6, 7), onChanged: (date) {
                        print('change $date');
                      }, onConfirm: (date) {
                        print('confirm $date');
                        dateCont.text = f.format(date);
                        print(dateCont.text);
                      }, currentTime: DateTime.now(), locale: LocaleType.en);
                    },
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                        errorStyle: TextStyle(height: 0.05),
                        hintText:
                            AppLocalizations.of(context).translate("Date"),
                        border: InputBorder.none,
                        suffixIcon: Icon(
                          Icons.date_range,
                          color: myColors.purple100,
                        ),
                        hintStyle: TextStyle(color: myColors.purple69)),
                    controller: dateCont,
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      border: Border.all(color: myColors.purple100)),
                ),
                SizedBox(
                  height: 17,
                ),

//                Padding(
//                  padding: EdgeInsets.only(right: 12,left: 12),
//                  child: Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                    children: [
//                      Column(
//                        children: [
//                          Text(
//                            AppLocalizations.of(context)
//                                .translate("Number Of Seats"),
//                            style: TextStyle(color: myColors.purple100),
//                          ),
//                          SizedBox(
//                            height: 10,
//                          ),
//                          Container(
//                            width: width / 3.5,
//                            child: TextFormField(
//                              validator: (v) {
//                                if (v != null && v.length == 0) {
//                                  return "        "+AppLocalizations.of(context)
//                                      .translate("Required");
//                                }
//                                return null;
//                              },
//                              textAlign: TextAlign.center,
//                              textInputAction: TextInputAction.done,
//                              keyboardType: TextInputType.number,
//                              inputFormatters: <TextInputFormatter>[
//                                FilteringTextInputFormatter.digitsOnly
//                              ],
//                              decoration: InputDecoration(
//                                  errorStyle: TextStyle(height: 0.05),
//
//                                  hintText: '20',
//                                  border: InputBorder.none,
//                                  hintStyle:
//                                      TextStyle(color: myColors.purple69)),
//                              controller: numberOfSeats,
//                            ),
//                            decoration: BoxDecoration(
//                                borderRadius: BorderRadius.circular(30),
//                                border: Border.all(color: myColors.purple100)),
//                          ),
//                        ],
//                      ),
//                      Column(
//                        children: [
//                          Text(
//                            AppLocalizations.of(context)
//                                .translate("Price Per Seat"),
//                            style: TextStyle(color: myColors.purple100),
//                          ),
//                          SizedBox(
//                            height: 10,
//                          ),
//                          Container(
//                            width: width / 3.5,
////                            padding: EdgeInsets.only(left: width/6, right: 20),
//                            child: TextFormField(
//                              validator: (v) {
//                                if (v != null && v.length == 0) {
//                                  return "        "+ AppLocalizations.of(context)
//                                      .translate("Required");
//                                }
//                                return null;
//                              },
//                              textAlign: TextAlign.center,
//                              textInputAction: TextInputAction.done,
//                              keyboardType: TextInputType.number,
//                              inputFormatters: <TextInputFormatter>[
//                                FilteringTextInputFormatter.digitsOnly
//                              ],
//                              decoration: InputDecoration(
//                                errorStyle: TextStyle(height: 0.05),
//                                  hintText: '20 SAR',
//                                  border: InputBorder.none,
//                                  hintStyle:
//                                      TextStyle(color: myColors.purple69)),
//                              controller: priceForOneSeat,
//                            ),
//                            decoration: BoxDecoration(
//                                borderRadius: BorderRadius.circular(30),
//                                border: Border.all(color: myColors.purple100)),
//                          ),
//                        ],
//                      ),
//
//                      Column(
//                        children: [
//                          Text(
//                            AppLocalizations.of(context)
//                                .translate("Number Of Seats"),
//                            style: TextStyle(color: myColors.purple100),
//                          ),
//                          SizedBox(
//                            height: 10,
//                          ),
//                          Container(
//                            width: width / 3.5,
//                            child: TextFormField(
//                              validator: (v) {
//                                if (v != null && v.length == 0) {
//                                  return "        "+AppLocalizations.of(context)
//                                      .translate("Required");
//                                }
//                                return null;
//                              },
//                              textAlign: TextAlign.center,
//                              textInputAction: TextInputAction.done,
//                              keyboardType: TextInputType.number,
//                              inputFormatters: <TextInputFormatter>[
//                                FilteringTextInputFormatter.digitsOnly
//                              ],
//                              decoration: InputDecoration(
//                                  errorStyle: TextStyle(height: 0.05),
//
//                                  hintText: '20',
//                                  border: InputBorder.none,
//                                  hintStyle:
//                                  TextStyle(color: myColors.purple69)),
//                              controller: numberOfSeats,
//                            ),
//                            decoration: BoxDecoration(
//                                borderRadius: BorderRadius.circular(30),
//                                border: Border.all(color: myColors.purple100)),
//                          ),
//                        ],
//                      ),
//
//                    ],
//                  ),
//                ),
                Column(
                    children: _getSeats().toList()),

                SizedBox(
                  height: 17,
                ),
                Container(
                  color: Colors.grey,
                  height: 1,
                  width: width - 16,
                ),
                SizedBox(
                  height: 17,
                ),
                Row(
                  children: [
                    Text(
                      AppLocalizations.of(context).translate("Location"),
                      style: TextStyle(fontSize: 18, color: myColors.purple100),
                    ),
                  ],
                ),
                SizedBox(
                  height: 12,
                ),
                InkWell(
                  onTap: () async {
                    result = await showLocationPicker(
                        context, "AIzaSyDQjshbHe8bemU7JxH5WT_or-vaxr-7LIw",
                        initialCenter: LatLng(33.33, 33.33),
                        automaticallyAnimateToCurrentLocation: true,
                        myLocationButtonEnabled: true,
                        language: Platform.localeName);
                    _controller
                        .moveCamera(CameraUpdate.newLatLng(result.latLng));
                  },
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: SizedBox(
                      height: 147,
                      width: width - 16,
                      child: Stack(
                        children: [
                          GoogleMap(
                            mapType: MapType.normal,
                            initialCameraPosition: CameraPosition(
                              target: LatLng(33.33, 33.33),
                              zoom: 14.4746,
                            ),
                            onMapCreated: (GoogleMapController controller) {
                              _controller = controller;
                            },
                            zoomControlsEnabled: false,
                          ),
                          Container(
                            child: Center(),
                            color: Color(0xFF0E3311).withOpacity(0.0),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  children: [
                    Text(
                      AppLocalizations.of(context).translate("Description"),
                      style: TextStyle(fontSize: 18, color: myColors.purple100),
                    ),
                  ],
                ),
                SizedBox(
                  height: 12,
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(color: myColors.purple100, spreadRadius: 1),
                    ],
                  ),
                  height: 130,
                  width: width - 16,
                  child: TextFormField(
                    validator: (v) {
                      if (v != null && v.length == 0) {
                        return AppLocalizations.of(context)
                            .translate("Please Enter description");
                      }
                      return null;
                    },
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    textInputAction: TextInputAction.newline,
                    decoration: InputDecoration(
                        hintText: AppLocalizations.of(context)
                            .translate("Description"),
                        border: InputBorder.none,
                        hintStyle: TextStyle(color: myColors.purple69)),
                    controller: description,
                  ),
                ),
                SizedBox(
                  height: 17,
                ),
                Row(
                  children: [
                    Text(
                      AppLocalizations.of(context).translate("Image"),
                      style: TextStyle(fontSize: 18, color: myColors.purple100),
                    ),
                  ],
                ),
                SizedBox(
                  height: 12,
                ),
                InkWell(
                  onTap: () {
                    _showPicker(context);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(color: myColors.purple100, spreadRadius: 1),
                      ],
                    ),
                    height: 150,
                    width: width - 16,
                    child: _image == null
                        ? Container(
                            child: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset(
                                      "assets/svg_icons/Icon feather-upload-cloud.svg"),
                                  Text(
                                    AppLocalizations.of(context)
                                        .translate("upload image"),
                                    style: TextStyle(color: myColors.purple100),
                                  )
                                ],
                              ),
                            ),
                          )
                        : ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: Image.file(
                              _image,
                              fit: BoxFit.cover,
                            ),
                          ),
                  ),
                ),
                SizedBox(
                  height: height / 29,
                ),
                Container(
                  width: width,
                  height: 38,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Color(0xff904e95),
                          Color(0xffe96443),
                        ],
                      ),
                      borderRadius: new BorderRadius.all(Radius.circular(8.0))),
                  child: TextButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        if (result != null) {
                          if (category != null) {
                            if (_image != null) {
                              buildShowDialog(context);
                              Provider.of<HomeViewModel>(context, listen: false)
                                  .addEvent(
                                      Event(
                                        name: eventName.text,
                                        address: result.address,
                                        category: category,
                                        seats: seats,
                                        description: description.text,
                                        date: DateTime.parse(dateCont.text),
                                        lat: result.latLng.latitude,
                                        lng: result.latLng.longitude,
                                        starName: starName.text,
                                      ),
                                      context,
                                      _image);
                            } else
                              showToast(
                                  AppLocalizations.of(context)
                                      .translate("Please upload Image"),
                                  context);
                          } else
                            showToast(
                                AppLocalizations.of(context)
                                    .translate("Please enter Event type"),
                                context);
                        } else
                          showToast(
                              AppLocalizations.of(context)
                                  .translate("Please enter location"),
                              context);
                      }
                    },
                    child: Text(
                      AppLocalizations.of(context).translate("Add Event"),
                      style: TextStyle(color: myColors.white, fontSize: 18),
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _getSeats() {
    List<Widget> friendsTextFieldsList = [];
    for (int i = 0; i < seats.length; i++) {
      friendsTextFieldsList.add(Padding(
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center
          ,
          children: [
            Expanded(child: SeatPage(i)),

            // we need add button at last friends row only
            _addRemoveButton(i == seats.length - 1, i),
          ],
        ),
      ));
    }
    return friendsTextFieldsList;
  }

  Widget _addRemoveButton(bool add, int index) {
    return InkWell(
      onTap: () {
        if (add) {
          // add new text-fields at the top of all friends textfields
          if(seats.length!=Provider.of<AddEventViewModel>(context,listen: false).seats.length)
          seats.insert(0, Seat());
        } else{
          seats.removeAt(index);
        }
        setState(() {});
      },
      child: Container(
        width: 30,
        height: 30,
        decoration: BoxDecoration(
          color: (add) ? Colors.green : Colors.red,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Icon(
          (add) ? Icons.add : Icons.remove,
          color: Colors.white,
        ),
      ),
    );
  }

  File _image;
  final picker = ImagePicker();

  Future _imgFromCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Future _imgFromGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text(
                          AppLocalizations.of(context).translate("Gallery")),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text(
                        AppLocalizations.of(context).translate("Camera")),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  void dispose() {
    seats=  [Seat()];
    super.dispose();
  }
}
