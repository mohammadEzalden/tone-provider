import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tonprovider/const/myColors.dart';
import 'package:tonprovider/customWidget/CustomClipper.dart';
import 'package:tonprovider/customWidget/image_from_network.dart';
import 'package:tonprovider/language/appLocalizations.dart';
import 'package:tonprovider/screens/home/trendingEvents.dart';
import 'package:tonprovider/view_models/home_view_model.dart';
import 'package:tonprovider/view_models/user_view_model.dart';

class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  final TextEditingController code = TextEditingController();
  String category = "all";
  DateFormat fDay ;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();
  @override
  void initState() {
    super.initState();
    if (Platform.isIOS) iOS_Permission();
    const AndroidInitializationSettings initializationSettingsAndroid =
    AndroidInitializationSettings('app_icon');
    final IOSInitializationSettings initializationSettingsIOS =
    IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    final MacOSInitializationSettings initializationSettingsMacOS =
    MacOSInitializationSettings();
    final InitializationSettings initializationSettings =
    InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS,
        macOS: initializationSettingsMacOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print(message);


        const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails('your channel id', 'your channel name',
            'your channel description',
            importance: Importance.max,
            priority: Priority.high,
            showWhen: false);
        const NotificationDetails platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);
        await flutterLocalNotificationsPlugin.show(
            0,
            message['notification']['title'],
            message['notification']['body'],
            platformChannelSpecifics,
            payload: 'shopper');
        Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
        Provider.of<HomeViewModel>(context, listen: false).getNotifies();
      },
      onLaunch: (Map<String, dynamic> message) async {
        print(message);
        Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();

        Provider.of<HomeViewModel>(context, listen: false).getNotifies();
      },
      onResume: (Map<String, dynamic> message) async {
        Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
        Provider.of<HomeViewModel>(context, listen: false).getNotifies();

        print(message);
      },
    );

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  Future selectNotification(String payload) async {
    Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
    Provider.of<HomeViewModel>(context, listen: false).getNotifies();
    if (payload != null) {
      debugPrint('notification payload2: $payload');
    }
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
    Provider.of<HomeViewModel>(context, listen: false).getNotifies();
  }

  Future<dynamic> onSelectNotification(String payload) async {
    Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
    Provider.of<HomeViewModel>(context, listen: false).getNotifies();
  }
  @override
  Widget build(BuildContext context) {
    fDay =
        new DateFormat("EEEE", AppLocalizations.of(context).locale.languageCode)
            .add_jm()
            .add_yMMMMd();

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0), // here the desired height
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.topRight,
                    colors: [
                      myColors.orange,
                      myColors.purple100,
                    ]),
              )),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 8, right: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: height / 29,
              ),
              Text(
                AppLocalizations.of(context).translate("Notification"),
                style: TextStyle(
                    fontSize: 18,
                    color: myColors.purple100,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: height / 29,
              ),
              Consumer<HomeViewModel>(builder: (context, home, child) {
                if (home.notifies.isEmpty)
                  return Padding(
                    padding: EdgeInsets.only(top: height / 4),
                    child: Center(
                        child: Text(
                          AppLocalizations.of(context)
                              .translate("You Don't have any Event"),
                          style: TextStyle(color: Colors.grey, fontSize: 20),
                        )),
                  );
                return Column(
                  children: home.notifies
                      .map((e) => Padding(
                    padding: const EdgeInsets.only(bottom: 9),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: myColors.purple69),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(11.0),
                        child: Column(
                          children: [
                            Text(
                              e.message,
                              style: TextStyle(
                                  color: myColors.purple100,
                                  fontSize: 20),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Divider(),
                            Text(fDay.format(e.date))
                          ],
                        ),
                      ),
                    ),
                  ))
                      .toList(),
                );
              }),
            ],
          ),
        ),
      ),
    );
  }

}
