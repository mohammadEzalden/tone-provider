import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tonprovider/const/myColors.dart';
import 'package:tonprovider/customWidget/image_from_network.dart';
import 'package:tonprovider/language/appLocalizations.dart';
import 'package:tonprovider/screens/home/eventDetails.dart';
import 'package:tonprovider/screens/home/trendingEvents.dart';
import 'package:tonprovider/view_models/addEventModel.dart';
import 'package:tonprovider/view_models/home_view_model.dart';
import 'package:tonprovider/view_models/user_view_model.dart';

class HomeBar extends StatefulWidget {
  @override
  _HomeBarState createState() => _HomeBarState();
}

class _HomeBarState extends State<HomeBar> {
  final TextEditingController query = TextEditingController();
  final TextEditingController dateCont = TextEditingController();
  String category = "all";
  String filterCategory;
  RangeValues _currentRangeValues = const RangeValues(1, 1000);

  DateFormat fDay;

  DateFormat fDate;

  DateFormat fTime;
  DateFormat f;

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();
  @override
  void initState() {
    super.initState();
    if (Platform.isIOS) iOS_Permission();
    const AndroidInitializationSettings initializationSettingsAndroid =
    AndroidInitializationSettings('app_icon');
    final IOSInitializationSettings initializationSettingsIOS =
    IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    final MacOSInitializationSettings initializationSettingsMacOS =
    MacOSInitializationSettings();
    final InitializationSettings initializationSettings =
    InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS,
        macOS: initializationSettingsMacOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print(message);


        const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails('your channel id', 'your channel name',
            'your channel description',
            importance: Importance.max,
            priority: Priority.high,
            showWhen: false);
        const NotificationDetails platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);
        await flutterLocalNotificationsPlugin.show(
            0,
            message['notification']['title'],
            message['notification']['body'],
            platformChannelSpecifics,
            payload: 'shopper');
        Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
        Provider.of<HomeViewModel>(context, listen: false).getNotifies();
      },
      onLaunch: (Map<String, dynamic> message) async {
        print(message);
        Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();

        Provider.of<HomeViewModel>(context, listen: false).getNotifies();
      },
      onResume: (Map<String, dynamic> message) async {
        Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
        Provider.of<HomeViewModel>(context, listen: false).getNotifies();

        print(message);
      },
    );

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  Future selectNotification(String payload) async {
    Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
    Provider.of<HomeViewModel>(context, listen: false).getNotifies();
    if (payload != null) {
      debugPrint('notification payload1: $payload');
    }
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
    Provider.of<HomeViewModel>(context, listen: false).getNotifies();
  }

  Future<dynamic> onSelectNotification(String payload) async {
    Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
    Provider.of<HomeViewModel>(context, listen: false).getNotifies();
  }
  @override
  Widget build(BuildContext context) {
    fDay = DateFormat("EEEE", AppLocalizations.of(context).locale.languageCode);
    fDate = DateFormat(
        'yyyy-MM-dd', AppLocalizations.of(context).locale.languageCode);
    fTime = DateFormat("jm", AppLocalizations.of(context).locale.languageCode);
    f = DateFormat(
        'yyyy-MM-dd hh:mm', AppLocalizations.of(context).locale.languageCode);
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0), // here the desired height
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
              decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.topRight,
                colors: [
                  myColors.orange,
                  myColors.purple100,
                ]),
          )),
        ),
      ),

      body:
      RefreshIndicator(
        onRefresh: refreshList,
        child: SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 8, right: 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: height / 29,
                    ),
                    SvgPicture.asset(
                      "assets/icons/tone.svg",
                      color: myColors.purple100,
                    ),
                    SizedBox(
                      height: height/29,
                    ),
                    Container(
                      width: width -16,
                      child: TextFormField(
                        textInputAction: TextInputAction.search,
                        decoration: InputDecoration(
                            prefixIcon: IconButton(
                              icon: SvgPicture.asset(
                                "assets/icons/search.svg",
                                height: 18,
                                width: 18,
                              ),
                            ),
//                          suffixIcon: IconButton(
//                            icon: SvgPicture.asset(
//                              "assets/icons/filter.svg",
//                              height: 19,
//                              width: 20,
//                            ),
//                            onPressed: () {
//
//                              _showPicker(context, width);
//                            },
//                          ),
                            hintText:
                            AppLocalizations.of(context).translate("Search"),
                            border: InputBorder.none,
                            hintStyle: TextStyle(color: myColors.purple69)),
                        controller: query,
                        onChanged: (v) {
                          Provider.of<HomeViewModel>(context, listen: false)
                              .filterBySearch( v);
                        },
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: myColors.gray)),
                    ),
                    SizedBox(
                      height: height/29,
                    ),
                    Text(
                      AppLocalizations.of(context).translate("Your Events"),
                      style: TextStyle(fontSize: 18, color: myColors.purple100),
                    ),
                    SizedBox(
                      height: 4,
                    ),
                  ],
                ),
              ),
              Consumer<HomeViewModel>(builder: (context, home, child) {
                if (home.eventsLoading)
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                if (home.events.isEmpty)
                  return Padding(
                    padding: EdgeInsets.only(top: height / 4),
                    child: Center(
                      child: Text(
                        AppLocalizations.of(context)
                            .translate("You Don't have any Event"),
                        style: TextStyle(color: Colors.grey, fontSize: 20),
                      ),
                    ),
                  );
                if (query.text.isNotEmpty)
                  return Padding(
                    padding: EdgeInsets.only(left: 8, right: 8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: home.eventsByCategory
                          .map((e) => InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => EventDetailsPage(
                                    e: e,
                                  )));
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(top: 7),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                height: height / 9,
                                width: width / 2.1,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: myColors.purple100,
                                ),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(8.0),
                                  child: ImageFromNetworkWidget(
                                    e.image,
                                    boxFit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 16,
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      e.name,
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold,
                                          color: myColors.purple100),
                                    ),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Text(
                                      fDay.format(e.date) +
                                          "\n" +
                                          fDate.format(e.date),
                                      style: TextStyle(
                                          color: myColors.purple100),
                                    ),
                                    Text(
                                      AppLocalizations.of(context)
                                          .translate("at") +
                                          " " +
                                          fTime.format(e.date),
                                      style: TextStyle(
                                          color: myColors.purple100),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ))
                          .toList(),
                    ),
                  );

                return Padding(
                  padding: EdgeInsets.only(left: 8, right: 8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: home.events
                        .map((e) => InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => EventDetailsPage(
                                  e: e,
                                )));
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(top: 7),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: height / 9,
                              width: width / 2.1,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: myColors.purple100,
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(8.0),
                                child: ImageFromNetworkWidget(
                                  e.image,
                                  boxFit: BoxFit.cover,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 16,
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    e.name,
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                        color: myColors.purple100),
                                  ),
                                  SizedBox(
                                    height: 8,
                                  ),
                                  Text(
                                    fDay.format(e.date) +
                                        "\n" +
                                        fDate.format(e.date),
                                    style: TextStyle(
                                        color: myColors.purple100),
                                  ),
                                  Text(
                                    AppLocalizations.of(context)
                                        .translate("at") +
                                        " " +
                                        fTime.format(e.date),
                                    style: TextStyle(
                                        color: myColors.purple100),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ))
                        .toList(),
                  ),
                );
              }),
              SizedBox(
                height: 100,
              )
            ],
          ),
        ),
      )
      ,
      floatingActionButton: Padding(
        padding:  EdgeInsets.only(bottom: 10),
        child: TextButton(
            style: ButtonStyle(
                padding: MaterialStateProperty.all(
                    EdgeInsets.symmetric(horizontal: width / 5, vertical: height/55)),
                backgroundColor: MaterialStateProperty.all(myColors.purple100),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.grey[300])))),
            onPressed: () {
//              Provider.of<AddEventViewModel>(context,listen: false).reset();

              Navigator.pushNamed(context, "/AddEventPage");
            },
            child: Text(
              AppLocalizations.of(context).translate("Add New Event"),
              style: TextStyle(color: myColors.white),
            )),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Future<void> refreshList() async {
    await Provider.of<HomeViewModel>(context, listen: false).getEvents();
  }
}
