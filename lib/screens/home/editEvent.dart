import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tonprovider/const/myColors.dart';
import 'package:tonprovider/const/widgets.dart';
import 'package:tonprovider/apis/api/api.dart' as api;
import 'package:tonprovider/customWidget/image_from_network.dart';
import 'package:tonprovider/customWidget/seatPageEdit.dart';
import 'package:tonprovider/language/appLocalizations.dart';
import 'package:tonprovider/model/event.dart';
import 'package:tonprovider/model/seat.dart';
import 'package:tonprovider/view_models/addEventModel.dart';
import 'package:tonprovider/view_models/home_view_model.dart';

class EditEventPage extends StatefulWidget {
  Event e;

  EditEventPage({this.e});

  @override
  EditEventPageState createState() => EditEventPageState();
}

class EditEventPageState extends State<EditEventPage> {
  final TextEditingController eventName = TextEditingController();
  final TextEditingController starName = TextEditingController();
  final TextEditingController dateCont = TextEditingController();
  final TextEditingController numberOfSeats = TextEditingController();
  final TextEditingController priceForOneSeat = TextEditingController();
  final TextEditingController description = TextEditingController();
  GoogleMapController _controller;
  final f = new DateFormat('yyyy-MM-dd HH:mm');
  int count = 1;
  String category;
  LocationResult result;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  static List<Seat> seats;

  @override
  void initState() {
    eventName.text = widget.e.name;
    starName.text = widget.e.starName;
    description.text = widget.e.description;
    numberOfSeats.text = widget.e.numberOfSeats.toString();
    priceForOneSeat.text = widget.e.priceForOneSeat.toString();
    dateCont.text = widget.e.date.toString();
    category = widget.e.category;
    seats=[];
    seats.addAll(widget.e.seats);
    result = LocationResult(
        latLng: LatLng(widget.e.lat, widget.e.lng), address: widget.e.address);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0), // here the desired height
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.topRight,
                    colors: [
                      myColors.orange,
                      myColors.purple100,
                    ]),
              )),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(left: 8, right: 8),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: height/29,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Row(
                        children: [
                          Icon(
                            Icons.arrow_back,
                            color: myColors.purple100,
                          ),
                          Text(
                            AppLocalizations.of(context)
                                .translate("Edit Event"),
                            style: TextStyle(
                                fontSize: 18,
                                color: myColors.purple100,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    IconButton(
                        icon: Icon(
                          Icons.delete,
                          color: Colors.red,
                        ),
                        onPressed: () {
                          deleteEvent(context, width, widget.e.id);
                        })
                  ],
                ),
                SizedBox(
                  height: height/33,
                ),
                Text(
                  AppLocalizations.of(context).translate("Event Name"),
                  style: TextStyle(color: myColors.purple100),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: width -16,
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: TextFormField(
                    validator: (v) {
                      if (v != null && v.length == 0) {
                        return AppLocalizations.of(context)
                            .translate("Please Enter Event Name");
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                        hintText:
                        AppLocalizations.of(context).translate("text"),
                        border: InputBorder.none,
                        hintStyle: TextStyle(color: myColors.purple69)),
                    controller: eventName,
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      border: Border.all(color: myColors.purple100)),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  AppLocalizations.of(context).translate("Type of event"),
                  style: TextStyle(color: myColors.purple100),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: width -16,
                  child: Consumer<HomeViewModel>(
                    builder: (context, home, child) {
                      if (!home.categories.contains(category)) category = null;
                      return DropdownButtonFormField(
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(
                                left: 20, top: 7, bottom: 7, right: 24),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  width: 1.0,
                                  style: BorderStyle.solid,
                                  color: myColors.purple100),
                              borderRadius:
                              BorderRadius.all(Radius.circular(30.0)),
                            ),
                            filled: true,
                            hintStyle: TextStyle(color: myColors.purple100),
                            hintText: AppLocalizations.of(context)
                                .translate("Type of event"),
                            fillColor: Colors.white),
                        value: category,
                        onChanged: (String Value) {
                          setState(() {
                            category = Value;
                          });
                        },
                        items: home.categories
                            .map((cityTitle) => DropdownMenuItem(
                            value: cityTitle,
                            child: Text(
                              "$cityTitle",
                              style: TextStyle(color: myColors.purple100),
                            )))
                            .toList(),
                      );
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 9, right: 9),
                  child: Text(
                    AppLocalizations.of(context).translate("Name of star"),
                    style: TextStyle(color: myColors.purple100),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: width-16,
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: TextFormField(
                    validator: (v) {
                      if (v != null && v.length == 0) {
                        return AppLocalizations.of(context)
                            .translate("Please Enter Star Name");
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                        errorStyle: TextStyle(height: 0.05),
                        hintText:
                        AppLocalizations.of(context).translate("text"),
                        border: InputBorder.none,
                        hintStyle: TextStyle(color: myColors.purple69)),
                    controller: starName,
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      border: Border.all(color: myColors.purple100)),
                ),
                SizedBox(
                  height: height/29,
                ),
                Container(
                  color: Colors.grey,
                  height: 1,
                  width: width -16,
                ),
                SizedBox(
                  height: height/29,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 9, right: 9),
                  child: Text(
                    AppLocalizations.of(context).translate("Date"),
                    style: TextStyle(color: myColors.purple100),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: width -16,
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: TextFormField(
                    readOnly: true,
                    validator: (v) {
                      if (v != null && v.length == 0) {
                        return AppLocalizations.of(context)
                            .translate("Please Enter Date");
                      }
                      return null;
                    },
                    onTap: () {
                      DatePicker.showDateTimePicker(context,
                          showTitleActions: true,
                          minTime: DateTime.now(),
                          maxTime: DateTime(2101, 6, 7), onChanged: (date) {
                            print('change $date');
                          }, onConfirm: (date) {
                            print('confirm $date');
                            dateCont.text = f.format(date);
                          }, currentTime: DateTime.now(), locale: LocaleType.en);
                    },
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                        errorStyle: TextStyle(height: 0.05),
                        hintText:
                        AppLocalizations.of(context).translate("Date"),
                        border: InputBorder.none,
                        suffixIcon: Icon(
                          Icons.date_range,
                          color: myColors.purple100,
                        ),
                        hintStyle: TextStyle(color: myColors.purple69)),
                    controller: dateCont,
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      border: Border.all(color: myColors.purple100)),
                ),
                SizedBox(
                  height: height/29,
                ),
                Column(
                    children: _getSeats().toList()),
                SizedBox(
                  height: height/29,
                ),
                Container(
                  color: Colors.grey,
                  height: 1,
                  width: width-16,
                ),
                SizedBox(
                  height: height/29,
                ),
                Text(
                  AppLocalizations.of(context).translate("Location"),
                  style: TextStyle(fontSize: 18, color: myColors.purple100),
                ),
                SizedBox(
                  height: 12,
                ),
                InkWell(
                  onTap: () async {
                    result = await showLocationPicker(
                        context, "AIzaSyDQjshbHe8bemU7JxH5WT_or-vaxr-7LIw",
                        initialCenter: LatLng(33.33, 33.33),
                        automaticallyAnimateToCurrentLocation: true,
                        myLocationButtonEnabled: true,
                        language: Platform.localeName);
                    _controller
                        .moveCamera(CameraUpdate.newLatLng(result.latLng));
                  },
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: SizedBox(
                      height: 147,
                      width: width - 16,
                      child: Stack(
                        children: [
                          GoogleMap(
                            mapType: MapType.normal,
                            initialCameraPosition: CameraPosition(
                              target: LatLng(33.33, 33.33),
                              zoom: 14.4746,
                            ),
                            onMapCreated: (GoogleMapController controller) {
                              _controller = controller;
                            },
                            zoomControlsEnabled: false,
                          ),
                          Container(
                            child: Center(),
                            color: Color(0xFF0E3311).withOpacity(0.0),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 12,
                ),
                Text(
                  AppLocalizations.of(context).translate("Description"),
                  style: TextStyle(fontSize: 18, color: myColors.purple100),
                ),
                SizedBox(
                  height: 12,
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(color: myColors.purple100, spreadRadius: 1),
                    ],
                  ),
                  height: 130,
                  width: width - 16,
                  child: TextFormField(
                    validator: (v) {
                      if (v != null && v.length == 0) {
                        return AppLocalizations.of(context)
                            .translate("Please Enter description");
                      }
                      return null;
                    },
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    textInputAction: TextInputAction.newline,
                    decoration: InputDecoration(
                        hintText: AppLocalizations.of(context)
                            .translate("Description"),
                        border: InputBorder.none,
                        hintStyle: TextStyle(color: myColors.purple69)),
                    controller: description,
                  ),
                ),
                SizedBox(
                  height: 12,
                ),
                Text(
                  AppLocalizations.of(context).translate("Image"),
                  style: TextStyle(fontSize: 18, color: myColors.purple100),
                ),
                SizedBox(
                  height: 12,
                ),
                InkWell(
                  onTap: () {
                    _showPicker(context);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(color: myColors.purple100, spreadRadius: 1),
                      ],
                    ),
                    height: _image == null ? 147 : width - 40,
                    width: width - 16,
                    child: _image == null
                        ? ClipRRect(                      borderRadius: BorderRadius.circular(10.0),
                        child: ImageFromNetworkWidget(widget.e.image,boxFit: BoxFit.cover,))
                        : ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: Image.file(
                        _image,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: height/20,
                ),
                Container(
                  width: width,
                  height: 38,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Color(0xff904e95),
                          Color(0xffe96443),
                        ],
                      ),
                      borderRadius: new BorderRadius.all(Radius.circular(8.0))),
                  child: TextButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        if (result != null) {
                          if (category != null) {
                            buildShowDialog(context);
                            Provider.of<HomeViewModel>(context, listen: false)
                                .editEvent(
                                Event(
                                  name: eventName.text,
                                  id: widget.e.id,
                                  image: widget.e.image,
                                  address: result.address,
                                  category: category,
                                  description: description.text,
                                  date: DateTime.parse(dateCont.text),
                                  lat: result.latLng.latitude,
                                  lng: result.latLng.longitude,
                                  seats: seats,
                                  
                                  starName: starName.text,
                                ),
                                context,
                                _image);
                          } else
                            showToast(
                                AppLocalizations.of(context)
                                    .translate("Please enter Event type"),
                                context);
                        } else
                          showToast(
                              AppLocalizations.of(context)
                                  .translate("Please enter location"),
                              context);
                      }
                    },
                    child: Text(
                      AppLocalizations.of(context).translate("Edit Event"),
                      style: TextStyle(color: myColors.white, fontSize: 18),
                    ),
                  ),
                ),
                SizedBox(
                  height: height/33,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _getSeats() {
    List<Widget> friendsTextFieldsList = [];
    for (int i = 0; i < seats.length; i++) {
      friendsTextFieldsList.add(Padding(
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center
          ,
          children: [
            Expanded(child: SeatPageEdit(i)),

            // we need add button at last friends row only
            _addRemoveButton(i == seats.length - 1, i),
          ],
        ),
      ));
    }
    return friendsTextFieldsList;
  }

  Widget _addRemoveButton(bool add, int index) {
    return InkWell(
      onTap: () {
        if (add) {
          // add new text-fields at the top of all friends textfields
          if(seats.length!=Provider.of<AddEventViewModel>(context,listen: false).seats.length)
            seats.insert(0, Seat());
        } else{
          seats.removeAt(index);
        }
        setState(() {});
      },
      child: Container(
        width: 30,
        height: 30,
        decoration: BoxDecoration(
          color: (add) ? Colors.green : Colors.red,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Icon(
          (add) ? Icons.add : Icons.remove,
          color: Colors.white,
        ),
      ),
    );
  }

  File _image;
  final picker = ImagePicker();

  Future _imgFromCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Future _imgFromGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text(
                          AppLocalizations.of(context).translate("Gallery")),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text(
                        AppLocalizations.of(context).translate("Camera")),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  deleteEvent(BuildContext context, double width, String id) {
    showDialog(
        context: context,
        barrierDismissible: false,
        barrierColor: Color(0xBFFFFFFF),
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              AppLocalizations.of(context).translate("Delete Event"),
              style: TextStyle(
                  color: myColors.purple100, fontWeight: FontWeight.bold),
            ),
            content: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return Container(
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          AppLocalizations.of(context).translate(
                              "Are you sure you want to delete this event?"),
                          style: TextStyle(color: myColors.purple100),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Container(
                                width: width/4,
                                padding: EdgeInsets.only(
                                    left: 8, right: 8, top: 12, bottom: 12),
                                child: Center(
                                  child: Text(
                                    AppLocalizations.of(context)
                                        .translate("Discard"),
                                    style: TextStyle(
                                        color: myColors.purple100,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    border:
                                    Border.all(color: myColors.purple100)),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            RawMaterialButton(
                              fillColor: myColors.purple100,
                              splashColor: myColors.purple100,
                              shape: StadiumBorder(
                                  side: BorderSide(color: myColors.purple100)),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: width/13, vertical: 15.0),
                                child: Text(
                                  AppLocalizations.of(context)
                                      .translate("Delete"),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              onPressed: () {
                                buildShowDialog(context);
                                Provider.of<HomeViewModel>(context,
                                    listen: false)
                                    .deleteEvent(id: id, context: context);
                              },
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0)), //this right here
          );
        });
  }
}
