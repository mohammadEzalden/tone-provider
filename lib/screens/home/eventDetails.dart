import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tonprovider/const/myColors.dart';
import 'package:tonprovider/const/widgets.dart';
import 'package:tonprovider/customWidget/MoveCameraPage.dart';
import 'package:tonprovider/customWidget/image_from_network.dart';
import 'package:tonprovider/language/appLocalizations.dart';
import 'package:tonprovider/model/event.dart';
import 'package:tonprovider/screens/home/editEvent.dart';
import 'package:tonprovider/view_models/user_view_model.dart';
import 'package:tonprovider/const/imageViwer.dart';

class EventDetailsPage extends StatelessWidget {
  final Event e;

  EventDetailsPage({this.e});

  final TextEditingController code = TextEditingController();
  GoogleMapController _controller;
  DateFormat fDay;

  DateFormat fDate;

  DateFormat fTime;

  @override
  Widget build(BuildContext context) {
    fDay = new DateFormat(
        "EEEE", AppLocalizations.of(context).locale.languageCode);
    fDate = new DateFormat(
        'yyyy-MM-dd', AppLocalizations.of(context).locale.languageCode);
    fTime =
        new DateFormat("jm", AppLocalizations.of(context).locale.languageCode);
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0), // here the desired height
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
              decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.topRight,
                colors: [
                  myColors.orange,
                  myColors.purple100,
                ]),
          )),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(left: 8, right: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: height / 29,
              ),
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Row(
                  children: [
                    Icon(
                      Icons.arrow_back,
                      color: myColors.purple100,
                    ),
                    Text(
                      AppLocalizations.of(context).translate("Event Details"),
                      style: TextStyle(
                          fontSize: 18,
                          color: myColors.purple100,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 7, right: 7),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: height / 29,
                    ),
                    Text(
                      e.name ?? " ",
                      style: TextStyle(
                          fontSize: 18,
                          color: myColors.purple100,
                          fontWeight: FontWeight.w400),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      e.category ?? " ",
                      style: TextStyle(
                          fontSize: 18,
                          color: myColors.purple100,
                          fontWeight: FontWeight.w400),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      e.starName ?? " ",
                      style: TextStyle(
                          fontSize: 18,
                          color: myColors.purple100,
                          fontWeight: FontWeight.w400),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                  ],
                ),
              ),
              InkWell(
                onTap: () {
//                  imageView(e.image);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ImageViewer(e.image)));
                },
                child: Center(
                  child: Container(
                    height: width / 1.5,
                    width: width / 1.1,
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(20.0),
                        child: ImageFromNetworkWidget(e.image,
                            boxFit: BoxFit.cover)),
                  ),
                ),
              ),
              SizedBox(
                height: 22,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 7, right: 7),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      fDay.format(e.date) + " " + fDate.format(e.date),
                      style: TextStyle(color: myColors.purple100),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      AppLocalizations.of(context).translate("at") +
                          " " +
                          fTime.format(e.date),
                      style: TextStyle(color: myColors.purple100),
                    ),
                    SizedBox(
                      height: height / 29,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              Text(
                                AppLocalizations.of(context)
                                    .translate("Available"),
                                style: TextStyle(color: myColors.purple100),
                              ),
                              SizedBox(height: 9,),

                              Column(
                                children: e.seats
                                    .map(
                                      (e) =>
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            bottom: 9),
                                        child: Text(
                                          e.number.toString(),
                                          style:
                                          TextStyle(
                                              color: myColors.purple100),
                                        ),
                                      ),
                                )
                                    .toList(),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Text(
                                AppLocalizations.of(context).translate(
                                    "Type"),
                                style: TextStyle(color: myColors.purple100),
                              ),
                              SizedBox(height: 9,),

                              Column(
                                children: e.seats
                                    .map(
                                      (e) =>
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            bottom: 9),
                                        child: Text(
                                          e.type,
                                          style:
                                          TextStyle(
                                              color: myColors.purple100),
                                        ),
                                      ),
                                )
                                    .toList(),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Text(
                                AppLocalizations.of(context).translate(
                                    "Price"),
                                style: TextStyle(color: myColors.purple100),
                              ),
                              SizedBox(height: 9,),
                              Column(
                                children: e.seats
                                    .map(
                                      (e) =>
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            bottom: 9),
                                        child: Text(
                                          e.price.toString() + " SAR",
                                          style:
                                          TextStyle(
                                              color: myColors.purple100),
                                        ),
                                      ),
                                )
                                    .toList(),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),

                    SizedBox(
                      height: 12,
                    ),
                    SizedBox(
                      height: 21,
                    ),
                    Text(
                      AppLocalizations.of(context).translate("Description"),
                      style: TextStyle(fontSize: 18, color: myColors.purple100),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      e.description,
                      style: TextStyle(color: myColors.purple100),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      AppLocalizations.of(context).translate("Location"),
                      style: TextStyle(fontSize: 18, color: myColors.purple100),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(color: myColors.purple100, spreadRadius: 1),
                        ],
                      ),
                      height: 147,
                      width: width - 16,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10.0),
                        child: GoogleMap(
                          onTap: (l) {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        MoveCamera(LatLng(e.lat, e.lng))));
                          },
                          mapType: MapType.normal,
                          initialCameraPosition: CameraPosition(
                            target: LatLng(e.lat, e.lng),
                            zoom: 14.4746,
                          ),
                          onMapCreated: (GoogleMapController controller) {
                            _controller = controller;
                          },
                          zoomControlsEnabled: false,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      AppLocalizations.of(context).translate("Address"),
                      style: TextStyle(fontSize: 18, color: myColors.purple100),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Text(
                        e.address,
                        style: TextStyle(color: myColors.purple100),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: height / 29,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      width: 90,
                      padding: EdgeInsets.only(
                          left: 8, right: 8, top: 12, bottom: 12),
                      child: Center(
                        child: Text(
                          AppLocalizations.of(context).translate("Back"),
                          style: TextStyle(
                              color: myColors.purple100,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: myColors.purple100)),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Consumer<UserViewModel>(
                    builder: (context, user, child) {
                      if (user.isLogin) return Container();
                      if (!user.isProvider)
                        return user.employee.role.edit_event
                            ? RawMaterialButton(
                                fillColor: myColors.purple100,
                                splashColor: myColors.purple100,
                                shape: StadiumBorder(
                                    side:
                                        BorderSide(color: myColors.purple100)),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 30.0, vertical: 15.0),
                                  child: Text(
                                    AppLocalizations.of(context)
                                        .translate("Edit"),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => EditEventPage(
                                                e: e,
                                              )));
                                },
                              )
                            : Container();
                      else
                        return RawMaterialButton(
                          fillColor: myColors.purple100,
                          splashColor: myColors.purple100,
                          shape: StadiumBorder(
                              side: BorderSide(color: myColors.purple100)),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 30.0, vertical: 15.0),
                            child: Text(
                              AppLocalizations.of(context).translate("Edit"),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => EditEventPage(
                                          e: e,
                                        )));
                          },
                        );
                    },
                  ),
                ],
              ),
              SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
