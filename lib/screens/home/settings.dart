import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tonprovider/const/myColors.dart';
import 'package:tonprovider/customWidget/image_from_network.dart';
import 'package:tonprovider/language/appLanguage.dart';
import 'package:tonprovider/language/appLocalizations.dart';
import 'package:tonprovider/view_models/user_view_model.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  String category;

  @override
  Widget build(BuildContext context) {
    category= AppLocalizations.of(context).locale.languageCode=="en"?"English":"Arabic";

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0), // here the desired height
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
              decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.topRight,
                colors: [
                  myColors.orange,
                  myColors.purple100,
                ]),
          )),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: height / 15,
            ),
            Consumer<UserViewModel>(
              builder: (context, user, child) {
                Widget imageWidget;
                if (user.user.imageUrl.isNotEmpty) {
                  imageWidget = Container(
                    height: 125.0,
                    width: 125.0,
                    decoration: new BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: const Color(0x33A6A6A6)),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10000.0),
                      child: ImageFromNetworkWidget(
                        user.user.imageUrl,
                        boxFit: BoxFit.cover,
                      ),
                    ),
                  );
                } else {
                  imageWidget = _image == null
                      ? Container(
                          height: 125.0,
                          width: 125.0,
                          decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              border:
                                  Border.all(color: const Color(0x33A6A6A6)),
                              image: DecorationImage(
                                  image:
                                      AssetImage("assets/images/person.jpeg"),
                                  fit: BoxFit.cover)),
                        )
                      : Container(
                          height: 125.0,
                          width: 125.0,
                          decoration: new BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(color: const Color(0x33A6A6A6)),
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10000.0),
                            child: Image.file(
                              _image,
                              fit: BoxFit.cover,
                            ),
                          ),
                        );
                }
                return Column(
                  children: [
                    Center(
                      child: Stack(
                        children: [
                          imageWidget,
//                          Positioned(
//                            top: 75,
////                   right: 75,
//                            left: -0,
//                            child: IconButton(
//                              onPressed: () {
//                                _showPicker(context);
//                              },
//                              icon: Icon(
//                                Icons.camera_alt,
//                                color: myColors.purple100,
//                                size: 43,
//                              ),
//                            ),
//                          ),
                        ],
                      ),
                    ),
                  ],
                );
              },
            ),
            SizedBox(
              height: height / 15,
            ),
            Row(
              children: [
                SizedBox(
                  width: 44,
                ),
                SvgPicture.asset(
                  "assets/icons/Icon ionic-ios-settings.svg",
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 24),
                  child: Text(
                    AppLocalizations.of(context).translate("Settings"),
                    style: TextStyle(fontSize: 18, color: myColors.purple100),
                  ),
                ),
                Expanded(child: Container()),
              ],
            ),
            SizedBox(
              height: height / 15,
            ),
            Padding(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    AppLocalizations.of(context).translate("Language") ,
                    style: TextStyle(fontSize: 18, color: myColors.purple100),
                  ),
                  Container(
                    width: width / 2.5,
                    child: DropdownButtonFormField(
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(
                              left: 20, top: 7, bottom: 7, right: 24),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                width: 1.0,
                                style: BorderStyle.solid,
                                color: myColors.purple100),
                            borderRadius:
                                BorderRadius.all(Radius.circular(11.0)),
                          ),
                          filled: true,
                          hintStyle: TextStyle(color: myColors.purple100),
                          fillColor: Colors.white),
                      value: category,
                      onChanged: (String value) {
                        setState(() {
                          category = value;
                          if(value=="English")
                          Provider.of<AppLanguage>(context,listen: false)
                              .changeLanguage(Locale("en"));
                          else Provider.of<AppLanguage>(context,listen: false)
                              .changeLanguage(Locale("ar"));
                        });
                      },
                      items: ["English", "Arabic"]
                          .map((cityTitle) => DropdownMenuItem(
                              value: cityTitle,
                              child: Text(
                                "$cityTitle",
                                style: TextStyle(color: myColors.purple100),
                              )))
                          .toList(),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          TextButton(
            onPressed: () async{
              SharedPreferences prefs = await SharedPreferences.getInstance();
              Provider.of<AppLanguage>(context,listen: false)
                  .changeLanguage(Locale(prefs.getString("language")));
              Navigator.pop(context);
            },
            child: Text(
              AppLocalizations.of(context).translate("Discard"),

              style: TextStyle(color: myColors.purple100, fontSize: 18),
            ),
          ),
          SizedBox(
            width: 17,
          ),
          Container(
            width: width / 3,
            height: 38,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color(0xff904e95),
                    Color(0xffe96443),
                  ],
                ),
                borderRadius: new BorderRadius.all(Radius.circular(8.0))),
            child: TextButton(
              onPressed: ()async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.setString("language", category=="English"?"en":"ar");
                Navigator.pop(context);

              },
              child: Text(
                AppLocalizations.of(context).translate("Save"),
                style: TextStyle(color: myColors.white, fontSize: 18),
              ),
            ),
          ),
        ],
      ),
    );
  }

  File _image;
  final picker = ImagePicker();

  Future _imgFromCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Future _imgFromGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text("Gallery"),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text("Camera"),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }
}
