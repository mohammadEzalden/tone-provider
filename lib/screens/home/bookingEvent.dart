import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tonprovider/const/myColors.dart';
import 'package:tonprovider/customWidget/CustomClipper.dart';
import 'package:tonprovider/customWidget/image_from_network.dart';
import 'package:tonprovider/language/appLocalizations.dart';
import 'package:tonprovider/screens/home/trendingEvents.dart';

class BookingEventPage extends StatefulWidget {
  @override
  _BookingEventPageState createState() => _BookingEventPageState();
}

class _BookingEventPageState extends State<BookingEventPage> {
  final TextEditingController code = TextEditingController();
  String category = "all";
  GoogleMapController _controller;
  int count=1;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0), // here the desired height
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.topRight,
                    colors: [
                      myColors.orange,
                      myColors.purple100,
                    ]),
              )),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 19, right: 18),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 60,
              ),
              InkWell(
                onTap: (){
                  Navigator.pop(context);
                },
                child: Row(
                  children: [
                    Icon(
                      Icons.arrow_back,
                      color: myColors.purple100,
                    ),
                    Text(
                      AppLocalizations.of(context).translate("Booking Ticket"),
                      style: TextStyle(
                          fontSize: 18,
                          color: myColors.purple100,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 22,
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(color: myColors.purple100, spreadRadius: 1),
                  ],
                ),
                height: height - 300,
                width: width - 40,
                child: Padding(
                  padding: const EdgeInsets.only(left: 17,right: 32),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 17,
                      ),
                      Text(
                        AppLocalizations.of(context).translate("Event"),
                        style: TextStyle(color: myColors.purple100),
                      ),
                      SizedBox(
                        height: 18,
                      ),
                      Text(
                        AppLocalizations.of(context).translate("Music Event"),
                        style: TextStyle(
                            fontSize: 18,
                            color: myColors.purple100,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 18,
                      ),
                      Text(
                        "Location",
                        style: TextStyle(color: myColors.purple100),
                      ),
                      SizedBox(
                        height: 13,
                      ),
                      Text(
                        "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs.",
                        style: TextStyle(color: myColors.purple100),
                      ),
                      SizedBox(
                        height: 21,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Date",
                                style: TextStyle(color: myColors.purple100),
                              ),
                              SizedBox(
                                height: 9,
                              ),
                              Text(
                                "14/4/2022",
                                style: TextStyle(color: myColors.purple100),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Time",
                                style: TextStyle(color: myColors.purple100),
                              ),
                              SizedBox(
                                height: 9,
                              ),
                              Text(
                                "10:00 am",
                                style: TextStyle(color: myColors.purple100),
                              ),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 21,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "ticket",
                            style: TextStyle(color: myColors.purple100),
                          ),
                          Row(
                            children: [
                              InkWell(
                                onTap: () {
                                  if (count > 1) {
                                    count--;
                                    setState(() {});
                                  }
                                },
                                child: Container(
                                  // width: 20,
                                  // height: 20,
                                  child: Center(
                                      child: Padding(
                                        padding:
                                        const EdgeInsets
                                            .all(3.0),
                                        child: Icon(
                                            Icons.remove,
                                            color: myColors
                                                .white,
                                            size: 15),
                                      )),
                                  decoration: BoxDecoration(
                                      color: myColors
                                          .purple100,
                                      borderRadius:
                                      BorderRadius.all(
                                          Radius
                                              .circular(
                                              3)),
                                      border: Border.all(
                                          color:myColors
                                              .purple100,
                                          width: 1)),
                                ),
                              ),
                              Padding(
                                padding:
                                const EdgeInsets.only(
                                    right: 8, left: 8),
                                child: Text("$count"),
                              ),
                              InkWell(
                                onTap: () {
                                  count++;
                                  setState(() {});
                                },
                                child: Container(
                                  // width: 20,
                                  // height: 20,
                                  child: Center(
                                      child: Padding(
                                        padding:
                                        const EdgeInsets
                                            .all(3.0),
                                        child: Icon(
                                          Icons.add,
                                          color: Colors
                                              .white,
                                          size: 15,
                                        ),
                                      )),
                                  decoration: BoxDecoration(
                                      color: myColors
                                          .purple100,
                                      borderRadius:
                                      BorderRadius.all(
                                          Radius
                                              .circular(
                                              3)),
                                      border: Border.all(
                                          color: myColors
                                              .purple100,
                                          width: 1)),
                                ),
                              ),
                              SizedBox(
                                width: 8,
                              ),

                            ],
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 32,
                      ),
                      Center(
                        child: Text(
                          "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -",
                          style: TextStyle(color: myColors.purple100),
                        ),

                      ),
                      SizedBox(
                        height: 16
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Price",
                            style: TextStyle(color: myColors.purple100),
                          ),
                          Text(
                            "\$00.00",
                            style: TextStyle(color: myColors.purple100),
                          ),
                        ],
                      ),

                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                width: width,
                height: 38,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Color(0xff904e95),
                        Color(0xffe96443),
                      ],
                    ),
                    borderRadius: new BorderRadius.all(Radius.circular(8.0))),
                child: TextButton(
                  onPressed: () {},
                  child: Text(
                    "Booking ticket",
                    style: TextStyle(color: myColors.white, fontSize: 18),
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
