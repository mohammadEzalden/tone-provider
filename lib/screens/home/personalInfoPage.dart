import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:tonprovider/const/myColors.dart';
import 'package:tonprovider/const/widgets.dart';
import 'package:tonprovider/customWidget/image_from_network.dart';
import 'package:tonprovider/language/appLocalizations.dart';
import 'package:tonprovider/model/user.dart';
import 'package:tonprovider/view_models/user_view_model.dart';

class PersonalInfoPage extends StatefulWidget {
  @override
  _PersonalInfoPageState createState() => _PersonalInfoPageState();
}

class _PersonalInfoPageState extends State<PersonalInfoPage> {
  final TextEditingController name = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController phone = TextEditingController();

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    final bool showFab = MediaQuery.of(context).viewInsets.bottom == 0.0;

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0), // here the desired height
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
              decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.topRight,
                colors: [
                  myColors.orange,
                  myColors.purple100,
                ]),
          )),
        ),
      ),
      body: SingleChildScrollView(
        child: Consumer<UserViewModel>(
          builder: (context, user, child) {
            if (user.user == null) {
              user.profileInfo();
              return Container();
            }
            if (phone.text.isEmpty && email.text.isEmpty && name.text.isEmpty) {
              phone.text = user.user.phone;
              email.text = user.user.email;
              name.text = user.user.name;
            }
            Widget imageWidget;
            if (user.user.imageUrl.isNotEmpty && _image == null) {
              imageWidget = Container(
                height: 125.0,
                width: 125.0,
                decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: const Color(0x33A6A6A6)),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10000.0),
                  child: ImageFromNetworkWidget(
                    user.user.imageUrl,
                    boxFit: BoxFit.cover,
                  ),
                ),
              );
            } else {
              imageWidget = _image == null
                  ? Container(
                      height: 125.0,
                      width: 125.0,
                      decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: const Color(0x33A6A6A6)),
                          image: DecorationImage(
                              image: AssetImage("assets/images/person.jpeg"),
                              fit: BoxFit.cover)),
                    )
                  : Container(
                      height: 125.0,
                      width: 125.0,
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: const Color(0x33A6A6A6)),
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10000.0),
                        child: Image.file(
                          _image,
                          fit: BoxFit.cover,
                        ),
                      ),
                    );
            }
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: height / 29,
                ),
                Center(
                  child: Stack(
                    children: [
                      imageWidget,
                      Positioned(
                        top: 75,
//                   right: 75,
                        left: -11,
                        child: IconButton(
                          onPressed: () {
                            _showPicker(context);
                          },
                          icon: Icon(
                            Icons.camera_alt,
                            color: myColors.purple100,
                            size: 43,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: height / 33,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: width / 15,
                    ),
                    SvgPicture.asset(
                      "assets/icons/account-circle.svg",
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 24),
                      child: Text(
                        AppLocalizations.of(context).translate("Personal Info"),
                        style:
                            TextStyle(fontSize: 18, color: myColors.purple100),
                      ),
                    ),
                    Expanded(child: Container()),
                  ],
                ),
                SizedBox(
                  height: height / 15,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 12, right: 12),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          SizedBox(
                            width: 12,
                          ),
                          SvgPicture.asset(
                            "assets/icons/username.svg",
                          ),
                          SizedBox(
                            width: 13,
                          ),
                          Text(
                            AppLocalizations.of(context).translate("UserName"),
                            style: TextStyle(
                                color: myColors.purple100, fontSize: 16),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 12, right: 12),
                        child: Container(
                          width: width - 16,
                          child: TextFormField(
                            validator: (v) {
                              if (v != null && v.length == 0) {
                                return "يرجى ادخال رمز التفعيل";
                              }
                              return null;
                            },
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                                hintText: 'test',
                                border: InputBorder.none,
                                hintStyle: TextStyle(
                                    color: myColors.purple100, fontSize: 12)),
                            controller: name,
                          ),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      color: myColors.purple100, width: 1))),
                        ),
                      ),
                      SizedBox(
                        height: height / 29,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 12,
                          ),
                          SvgPicture.asset(
                            "assets/icons/email.svg",
                          ),
                          SizedBox(
                            width: 13,
                          ),
                          Text(
                            AppLocalizations.of(context).translate("Email"),
                            style: TextStyle(
                                color: myColors.purple100, fontSize: 16),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 12, right: 12),
                        child: Container(
                          width: width - 16,
                          child: TextFormField(
                            validator: (v) {
                              if (v != null && v.length == 0) {
                                return "يرجى ادخال رمز التفعيل";
                              }
                              return null;
                            },
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                                hintText: 'test@mail.com',
                                border: InputBorder.none,
                                hintStyle: TextStyle(
                                    color: myColors.purple100, fontSize: 12)),
                            controller: email,
                          ),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      color: myColors.purple100, width: 1))),
                        ),
                      ),
                      SizedBox(
                        height: height / 29,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 12,
                          ),
                          SvgPicture.asset(
                            "assets/icons/phone.svg",
                          ),
                          SizedBox(
                            width: 13,
                          ),
                          Text(
                            AppLocalizations.of(context).translate("Phone"),
                            style: TextStyle(
                                color: myColors.purple100, fontSize: 16),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 12, right: 12),
                        child: Container(
                          width: width - 16,
                          child: TextFormField(
                            validator: (v) {
                              if (v != null && v.length == 0) {
                                return "يرجى ادخال رمز التفعيل";
                              }
                              return null;
                            },
                            textInputAction: TextInputAction.done,
                            keyboardType: TextInputType.phone,
                            decoration: InputDecoration(
                                hintText: '+963224578876',
                                border: InputBorder.none,
                                hintStyle: TextStyle(
                                    color: myColors.purple100, fontSize: 12)),
                            controller: phone,
                          ),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      color: myColors.purple100, width: 1))),
                        ),
                      ),
                      SizedBox(height: height / 29),
                    ],
                  ),
                ),
              ],
            );
          },
        ),
      ),
      floatingActionButton: showFab
          ? Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text(
                    AppLocalizations.of(context).translate("Discard"),
                    style: TextStyle(color: myColors.purple100, fontSize: 18),
                  ),
                ),
                SizedBox(
                  width: 17,
                ),
                Container(
                  width: width / 3,
                  height: 38,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Color(0xff904e95),
                          Color(0xffe96443),
                        ],
                      ),
                      borderRadius: new BorderRadius.all(Radius.circular(8.0))),
                  child: TextButton(
                    onPressed: () {
                      buildShowDialog(context);
                      Provider.of<UserViewModel>(context, listen: false)
                          .editUser(
                              User(
                                  name: name.text,
                                  email: email.text,
                                  phone: phone.text),
                              image: _image,
                              context: context);
                    },
                    child: Text(
                      AppLocalizations.of(context).translate("Save"),
                      style: TextStyle(color: myColors.white, fontSize: 18),
                    ),
                  ),
                ),
              ],
            )
          : Container(),
    );
  }

  File _image;
  final picker = ImagePicker();

  Future _imgFromCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Future _imgFromGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text(
                          AppLocalizations.of(context).translate("Gallery")),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text(
                        AppLocalizations.of(context).translate("Camera")),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }
}
