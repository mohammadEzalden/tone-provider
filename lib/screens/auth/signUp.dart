import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tonprovider/const/myColors.dart';
import 'package:tonprovider/const/widgets.dart';
import 'package:tonprovider/customWidget/CustomClipperSignUp.dart';
import 'package:tonprovider/apis/auth/signupApi.dart' as api;
import 'package:tonprovider/language/appLocalizations.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final TextEditingController username = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController password = TextEditingController();
  final TextEditingController phone = TextEditingController();
  final TextEditingController confirmPassword = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 267,
              child: ClipPath(
                clipper: CustomClipPathSignUp(),
                child: Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          colors: [
                            myColors.orange,
                            myColors.purple100,

//                    Color(0xffFCAF45),
//                        Color(0xffF58529),
                          ]),
                    ),
                    child: Padding(
                      padding:  EdgeInsets.only(left: width/13),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 60,
                              ),
                              SvgPicture.asset("assets/icons/tone.svg"),
                              SizedBox(
                                height: 20,
                              ),
                              Container(
                                color: Colors.white,
                                height: 2,
                                width: 64,
                              ),
                              SizedBox(
                                height: 45,
                              ),
                              Text(
                                AppLocalizations.of(context).translate("signup"),
                                style: TextStyle(
                                    color: Colors.white, fontSize: 30),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )),
              ),
            ),
            SizedBox(
              height: height/30,
            ),
            Padding(
              padding:  EdgeInsets.only(left: 12,right:12),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        SvgPicture.asset(
                          "assets/icons/username.svg",
                        ),
                        SizedBox(
                          width: 13,
                        ),
                        Text(
                          AppLocalizations.of(context).translate("UserName"),
                          style: TextStyle(
                              color: myColors.purple100, fontSize: 16),
                        ),
                      ],
                    ),
                    Padding(
                      padding:  EdgeInsets.only(left: 27,right: 27),
                      child: Container(
                        width: width-30,
                        child: TextFormField(
//                      validator: (v) {
//                        if (v != null && v.length == 0) {
//                          return "يرجى ادخال رمز التفعيل";
//                        }
//                        return null;
//                      },
                          textInputAction: TextInputAction.done,
                          decoration: InputDecoration(
                              hintText: AppLocalizations.of(context).translate("UserName"),
                              border: InputBorder.none,
                              hintStyle: TextStyle(
                                  color: myColors.purple100, fontSize: 12)),
                          controller: username,
                        ),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: myColors.purple100, width: 1))),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        SvgPicture.asset(
                          "assets/icons/email.svg",
                        ),
                        SizedBox(
                          width: 13,
                        ),
                        Text(
                          AppLocalizations.of(context).translate("Email"),
                          style: TextStyle(
                              color: myColors.purple100, fontSize: 16),
                        ),
                      ],
                    ),
                    Padding(
                      padding:  EdgeInsets.only(left: 27,right: 27),

                      child: Container(
                        width: width-30,
                        child: TextFormField(
                          validator: (v) {
                            if (v != null && v.length == 0) {
                              return AppLocalizations.of(context).translate("Please Enter Email");
                            }
                            if (!EmailValidator.validate(email.text)) {
                              return AppLocalizations.of(context)
                                  .translate("Email isn't valid");
                            }
                            return null;
                          },
                          textInputAction: TextInputAction.done,
                          decoration: InputDecoration(
                              hintText: 'test@mail.com',
                              border: InputBorder.none,
                              hintStyle: TextStyle(
                                  color: myColors.purple100, fontSize: 12)),
                          controller: email,
                        ),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: myColors.purple100, width: 1))),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        SvgPicture.asset(
                          "assets/icons/phone.svg",
                        ),
                        SizedBox(
                          width: 13,
                        ),
                        Text(
                           AppLocalizations.of(context).translate("Phone"),
                          style: TextStyle(
                              color: myColors.purple100, fontSize: 16),
                        ),
                      ],
                    ),
                    Padding(
                      padding:  EdgeInsets.only(left: 27,right: 27),

                      child: Container(
                        width: width-30,
                        child: TextFormField(
//                        validator: (v) {
//                          if (v != null && v.length == 0) {
//                            return "يرجى ادخال رمز التفعيل";
//                          }
//                          return null;
//                        },
                          textInputAction: TextInputAction.done,
                          keyboardType: TextInputType.phone,
                          decoration: InputDecoration(
                              hintText: '011234567890',
                              border: InputBorder.none,
                              hintStyle: TextStyle(
                                  color: myColors.purple100, fontSize: 12)),
                          controller: phone,
                        ),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: myColors.purple100, width: 1))),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        SvgPicture.asset(
                          "assets/icons/password.svg",
                        ),
                        SizedBox(
                          width: 13,
                        ),
                        Text(
                          AppLocalizations.of(context).translate("Password"),
                          style: TextStyle(
                              color: myColors.purple100, fontSize: 16),
                        ),
                      ],
                    ),
                    Padding(
                      padding:  EdgeInsets.only(left: 27,right: 27),

                      child: Container(
                        width: width-30,
                        child: TextFormField(
                          validator: (v) {
                            if (v != null && v.length == 0) {
                              return AppLocalizations.of(context).translate("Please Enter Password");
                            }
                            return null;
                          },
                          textInputAction: TextInputAction.done,
                          obscureText: true,
                          decoration: InputDecoration(
                              hintText: '*********',
                              border: InputBorder.none,
                              hintStyle: TextStyle(
                                  color: myColors.purple100, fontSize: 12)),
                          controller: password,
                        ),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: myColors.purple100, width: 1))),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        SvgPicture.asset(
                          "assets/icons/password.svg",
                        ),
                        SizedBox(
                          width: 13,
                        ),
                        Text(
                          AppLocalizations.of(context).translate('Confirm Password'),
                          style: TextStyle(
                              color: myColors.purple100, fontSize: 16),
                        ),
                      ],
                    ),
                    Padding(
                      padding:  EdgeInsets.only(left: 27,right: 27),

                      child: Container(
                        width: width-30,
                        child: TextFormField(
                          validator: (v) {
                            if (v != null && v.length == 0) {
                              return AppLocalizations.of(context).translate('Please Enter Password');
                            }
                            return null;
                          },
                          textInputAction: TextInputAction.done,
                          obscureText: true,
                          decoration: InputDecoration(
                              hintText: '*********',
                              border: InputBorder.none,
                              hintStyle: TextStyle(
                                  color: myColors.purple100, fontSize: 12)),
                          controller: confirmPassword,
                        ),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: myColors.purple100, width: 1))),
                      ),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.check_box,
                          color: myColors.purple100,
                          size: 17,
                        ),
                        SizedBox(
                          width: 13,
                        ),
                        Text(
                          AppLocalizations.of(context).translate('You agree with our '),
                          style: TextStyle(color: myColors.purple100),
                        ),
                        InkWell(
                          onTap: (){
                            launchURL("https://toneap.com/terms");
                          },
                          child: Container(
                            padding: EdgeInsets.only(
                              bottom: 3, // Space between underline and text
                            ),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                              color: myColors.orange,
                              width: 1.0, // Underline thickness
                            ))),
                            child: Text(
                              AppLocalizations.of(context).translate("Terms & Conditions"),
                              style: TextStyle(
                                color: myColors.orange,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 38,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    if (_formKey.currentState.validate()) {
                      if(password.text.trim()!=confirmPassword.text.trim()){
                        showToast(AppLocalizations.of(context).translate("Passwords does not match"), context);
                        return;
                      }
                      buildShowDialog(context);
                      api.signup(
                          phone: phone.text,
                          email: email.text.trim(),
                          password: password.text.trim(),
                          context: context,
                          username: username.text);
                    }
                  },
                  child: Container(
                    height: 44,
                    width: 140,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          colors: [
                            myColors.purple100,
                            myColors.orange,
                          ]),
                    ),
                    child: Center(
                        child: Text(
                      AppLocalizations.of(context).translate("SIGNUP"),
                      style: TextStyle(color: myColors.white, fontSize: 16),
                    )),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 23,
            ),
          ],
        ),
      ),
    );
  }

  buildShowDialog(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }
}
