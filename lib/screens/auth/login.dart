import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tonprovider/apis/auth/loginApi.dart' as api;
import 'package:tonprovider/const/myColors.dart';
import 'package:tonprovider/const/widgets.dart';
import 'package:tonprovider/customWidget/CustomClipper.dart';
import 'package:tonprovider/language/appLocalizations.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController email = TextEditingController();
  final TextEditingController password = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Container(
        height: height,
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                myColors.orange,
                myColors.purple100,
              ]),
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(left: width/13, right: width/13),
            child: Form(
              key: _formKey,
              child: Theme(
                data: ThemeData(errorColor: Colors.white),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: height / 5,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 13,
                        ),
                        SvgPicture.asset("assets/icons/tone.svg"),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 13,
                        ),
                        Container(
                          color: Colors.white,
                          height: 2,
                          width: 64,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: height /29,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: 13,
                        ),
                        Text(
                          AppLocalizations.of(context).translate("Email"),
                          style: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                      ],
                    ),
                    Container(
                      width: width / 1.3,
                      child: TextFormField(
                        validator: (v) {
                          if (v != null && v.length == 0) {
                            return AppLocalizations.of(context)
                                .translate("Please Enter Email");
                          }
                          if (!EmailValidator.validate(email.text)) {
                            return AppLocalizations.of(context)
                                .translate("Email isn't valid");
                          }
                          return null;
                        },
                        textInputAction: TextInputAction.done,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            hintText: 'test@mail.com',
                            border: InputBorder.none,
                            hintStyle: TextStyle(color: Colors.white)),
                        controller: email,
                      ),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom:
                                  BorderSide(color: Colors.white, width: 1))),
                    ),
                    SizedBox(
                      height: height/20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,

                      children: [
                        SizedBox(
                          width: 13,
                        ),
                        Text(
                          AppLocalizations.of(context).translate("Password"),
                          style: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                      ],
                    ),
                    Container(
                      width: width / 1.3,
                      child: TextFormField(
                        style: TextStyle(color: Colors.white),
                        validator: (v) {
                          if (v != null && v.length == 0) {
                            return AppLocalizations.of(context)
                                .translate("Please Enter Password");
                          }
                          return null;
                        },
                        obscureText: true,
                        textInputAction: TextInputAction.done,
                        decoration: InputDecoration(
                            counterStyle: TextStyle(color: Colors.white),
                            border: InputBorder.none,
                            hintText: "*************",
                            hintStyle: TextStyle(color: Colors.white)),
                        controller: password,
                      ),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom:
                                  BorderSide(color: Colors.white, width: 1))),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        InkWell(
                          onTap: () {
                            if (email.text.isEmpty) {
                              showToast(
                                  AppLocalizations.of(context)
                                      .translate("Please Enter Email"),
                                  context);
                              return;
                            }
                            if (!EmailValidator.validate(email.text)) {
                              showToast(
                                  AppLocalizations.of(context)
                                      .translate("Email isn't valid"),
                                  context);
                              return;
                            }
                            buildShowDialog(context);
                            api.forgetPassword(
                                context: context, email: email.text);
                          },
                          child: Text(
                            AppLocalizations.of(context)
                                .translate("Forget Password?"),
                            style: TextStyle(color: Colors.white, fontSize: 12),
                          ),
                        ),
                        SizedBox(
                          width: width / 13,
                        )
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      height: height / 17,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextButton(
                            onPressed: () async {
                              if (_formKey.currentState.validate()) {
                                buildShowDialog(context);
                                api.login(
                                    context: context,
                                    email: email.text.trim(),
                                    password: password.text.trim());
                              }
                            },
                            child: Text(
                              AppLocalizations.of(context).translate("Login"),
                              style: TextStyle(color: myColors.purple85),
                            ),
                            style: ButtonStyle(
                                padding: MaterialStateProperty.all(
                                    EdgeInsets.symmetric(
                                        horizontal: width / 8, vertical: 13)),
                                backgroundColor:
                                    MaterialStateProperty.all(myColors.white),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(18.0),
                                        side: BorderSide(
                                            color: Colors.grey[300]))))),
                        SizedBox(
                          width: width / 13,
                        ),
                        TextButton(
                            onPressed: () {
                              Navigator.pushNamed(context, "/SignUpPage");
                            },
                            child: Text(
                              AppLocalizations.of(context).translate("SignUp"),
                              style: TextStyle(color: myColors.purple69),
                            ),
                            style: ButtonStyle(
                                padding: MaterialStateProperty.all(
                                    EdgeInsets.symmetric(
                                        horizontal: width / 9, vertical: 13)),
                                backgroundColor:
                                    MaterialStateProperty.all(myColors.white),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(18.0),
                                        side: BorderSide(
                                            color: Colors.grey[300]))))),
                      ],
                    ),
                    SizedBox(
                      height: height/3.9,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Text(
                            AppLocalizations.of(context).translate(
                                "By signing up, you agree with our "),
                            style: TextStyle(color: Colors.white, fontSize: 12),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            launchURL("https://toneap.com/terms");
                          },
                          child: Container(
                            padding: EdgeInsets.only(
                              bottom: 3, // Space between underline and text
                            ),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                              color: myColors.white,
                              width: 1.0, // Underline thickness
                            ))),
                            child: Text(
                              AppLocalizations.of(context)
                                  .translate("Terms & Conditions"),
                              style: TextStyle(
                                  color: myColors.white, fontSize: 12),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: height / 20,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  buildShowDialog(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }
}
