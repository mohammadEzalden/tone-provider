import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:tonprovider/StaffPart/qrCodeScan.dart';
import 'package:tonprovider/const/myColors.dart';
import 'package:tonprovider/screens/auth/login.dart';
import 'package:tonprovider/screens/auth/signUp.dart';
import 'package:tonprovider/screens/home/addEvent.dart';
import 'package:tonprovider/screens/home/bookingEvent.dart';
import 'package:tonprovider/screens/home/eventDetails.dart';
import 'package:tonprovider/screens/home/personalInfoPage.dart';
import 'package:tonprovider/screens/home/profilePage.dart';
import 'package:tonprovider/screens/home/settings.dart';
import 'package:tonprovider/screens/home/staffPage.dart';
import 'package:tonprovider/splash.dart';
import 'package:tonprovider/view_models/addEventModel.dart';
import 'package:tonprovider/view_models/emolyeeViewModel.dart';
import 'package:tonprovider/view_models/home_view_model.dart';
import 'package:tonprovider/view_models/user_view_model.dart';

import 'StaffPart/homeEmployeePage.dart';
import 'home.dart';
import 'language/appLanguage.dart';
import 'language/appLocalizations.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
//  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
//  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
//      FlutterLocalNotificationsPlugin();
  final AppLanguage appLanguage;

  _MyAppState({this.appLanguage});
//
//  @override
//  void initState() {
//    super.initState();
//    if (Platform.isIOS) iOS_Permission();
//// initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
//    const AndroidInitializationSettings initializationSettingsAndroid =
//        AndroidInitializationSettings('app_icon');
//    final IOSInitializationSettings initializationSettingsIOS =
//        IOSInitializationSettings(
//            onDidReceiveLocalNotification: onDidReceiveLocalNotification);
//    final MacOSInitializationSettings initializationSettingsMacOS =
//        MacOSInitializationSettings();
//    final InitializationSettings initializationSettings =
//        InitializationSettings(
//            android: initializationSettingsAndroid,
//            iOS: initializationSettingsIOS,
//            macOS: initializationSettingsMacOS);
//    flutterLocalNotificationsPlugin.initialize(initializationSettings,
//        onSelectNotification: selectNotification);
//
//    _firebaseMessaging.configure(
//      onMessage: (Map<String, dynamic> message) async {
//        print(message);
//        print(context);
//        print(Provider.of<UserViewModel>(context,listen: false).runtimeType);
//        print(message);
//
//        const AndroidNotificationDetails androidPlatformChannelSpecifics =
//            AndroidNotificationDetails('your channel id', 'your channel name',
//                'your channel description',
//                importance: Importance.max,
//                priority: Priority.high,
//                showWhen: false);
//        const NotificationDetails platformChannelSpecifics =
//            NotificationDetails(android: androidPlatformChannelSpecifics);
//        await flutterLocalNotificationsPlugin.show(
//            0,
//            message['notification']['title'],
//            message['notification']['body'],
//            platformChannelSpecifics,
//            payload: 'shopper');
//        Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
//        Provider.of<HomeViewModel>(context, listen: false).getNotifies();
//      },
//      onLaunch: (Map<String, dynamic> message) async {
//        print(message);
//        Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
//
//        Provider.of<HomeViewModel>(context, listen: false).getNotifies();
//      },
//      onResume: (Map<String, dynamic> message) async {
//        Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
//        Provider.of<HomeViewModel>(context, listen: false).getNotifies();
//
//        print(message);
//      },
//    );
//
//    _firebaseMessaging.requestNotificationPermissions(
//        const IosNotificationSettings(
//            sound: true, badge: true, alert: true, provisional: true));
//    _firebaseMessaging.onIosSettingsRegistered
//        .listen((IosNotificationSettings settings) {
//      print("Settings registered: $settings");
//    });
//  }
//
//  void iOS_Permission() {
//    _firebaseMessaging.requestNotificationPermissions(
//        IosNotificationSettings(sound: true, badge: true, alert: true));
//    _firebaseMessaging.onIosSettingsRegistered
//        .listen((IosNotificationSettings settings) {
//      print("Settings registered: $settings");
//    });
//  }
//
//  Future selectNotification(String payload) async {
//    if (payload != null) {
//      debugPrint('notification payload: $payload');
//    }
//  }
//
//  Future onDidReceiveLocalNotification(
//      int id, String title, String body, String payload) async {
//    // display a dialog with the notification details, tap ok to go to another page
//  }
//
//  Future<dynamic> onSelectNotification(String payload) async {
//    /*Do whatever you want to do on notification click. In this case, I'll show an alert dialog*/
//  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => AddEventViewModel()),
        ChangeNotifierProvider(create: (context) => AppLanguage()),
        ChangeNotifierProvider(create: (context) => HomeViewModel()),
        ChangeNotifierProvider(create: (context) => EmployeeViewModel()),
        ChangeNotifierProvider(create: (context) => UserViewModel()),
      ],
      child: Consumer<AppLanguage>(
        builder: (context, model, child) {
          return MaterialApp(
            locale: model.appLocal,
            localizationsDelegates: [
              AppLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            supportedLocales: [
              Locale("ar", 'AE'),
              Locale("en", 'US'),
              Locale("fr", 'FR'),
              Locale("hi", 'IN'),
              Locale("fa", 'IR'),
            ],

            //title:  getTranslated(context, 'Employees'),
            theme: ThemeData(
                primaryColor: myColors.purple100,
                accentColor: myColors.purple100,
                fontFamily: "Ubuntu"),
            home: Splash(),

            routes: {
              '/LoginPage': (context) => LoginPage(),
              '/SignUpPage': (context) => SignUpPage(),
              '/HomePage': (context) => HomePage(),
              '/HomeEmployeePage': (context) => HomeEmployeePage(),
              '/ProfilePage': (context) => ProfilePage(),
              '/EventDetailsPage': (context) => EventDetailsPage(),
              '/BookingEventPage': (context) => BookingEventPage(),
              '/AddEventPage': (context) => AddEventPage(),
              '/StaffPage': (context) => StaffPage(),
              '/QRCodeScanPage': (context) => QRCodeScanPage(),
              '/SettingsPage': (context) => SettingsPage(),
              '/PersonalInfoPage': (context) => PersonalInfoPage(),
            },
          );
        },
      ),
    );
  }
}

//AppLocalizations.of(context).translate("Next")

///flutter build apk --release
///flutter build apk --split-per-abi
///taskkill /F /IM dart.exe
///flutter pub run flutter_launcher_icons:main

//SH1
///cd C:\Program Files (x86)\Java\jdk1.8.0_144\bin
///keytool -list -v -alias androiddebugkey -keystore C:\Users\m-n-3\.android\debug.keystore
///keytool -list -v -keystore C:\Users\m-n-3\key.jks -alias key

//keytool -exportcert -alias androiddebugkey -keystore "C:\Users\m-n-3\.android\debug.keystore" | "C:\Users\m-n-3\Downloads\sss\bin\openssl" sha1 -binary | "C:\Users\m-n-3\Downloads\sss\bin\openssl" base64

//key store
///https://stackoverflow.com/questions/51168235/how-to-get-signed-apk-for-flutter-with-existing-app-keystore
///https://share-my-key.firebaseapp.com/__/auth/handler
