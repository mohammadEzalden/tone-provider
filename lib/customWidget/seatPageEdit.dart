import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:tonprovider/const/myColors.dart';
import 'package:tonprovider/language/appLocalizations.dart';
//import 'package:tonprovider/screens/home/addEvent.dart';
import 'package:tonprovider/view_models/addEventModel.dart';
import 'package:tonprovider/screens/home/editEvent.dart';
class SeatPageEdit extends StatefulWidget {
  final int index;

  const SeatPageEdit(this.index);

  @override
  _SeatPageEditState createState() => _SeatPageEditState();
}

class _SeatPageEditState extends State<SeatPageEdit> {
  final TextEditingController number = TextEditingController();
  final TextEditingController price = TextEditingController();
  String type;

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      price.text = EditEventPageState.seats[widget.index].price != null
          ? EditEventPageState.seats[widget.index].price.toString()
          : '';
      number.text = EditEventPageState.seats[widget.index].number != null
          ? EditEventPageState.seats[widget.index].number.toString()
          : '';
      type = EditEventPageState.seats[widget.index].type;
    });
    double width = MediaQuery.of(context).size.width;
    return Padding(
      padding: EdgeInsets.only(right: 12, left: 12),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: [
                  Text(
                    AppLocalizations.of(context).translate("Number Of Seats"),
//                    "Number",
                    style: TextStyle(color: myColors.purple100),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    width: width / 4,
                    child: TextFormField(
                      validator: (v) {
                        if (v != null && v.length == 0) {
                          return "        " +
                              AppLocalizations.of(context).translate("Required");
                        }
                        return null;
                      },
                      onChanged: (v) {
                        try {
                          EditEventPageState.seats[widget.index].number =
                              int.parse(v);
                        } catch (e) {}
                      },
                      textAlign: TextAlign.center,
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly
                      ],
                      decoration: InputDecoration(
                          errorStyle: TextStyle(height: 0.05),
                          hintText: '20',
                          border: InputBorder.none,
                          hintStyle: TextStyle(color: myColors.purple69)),
                      controller: number,
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        border: Border.all(color: myColors.purple100)),
                  ),
                ],
              ),
              Column(
                children: [
                  Text(
                    AppLocalizations.of(context).translate("Price Per Seat"),
                    style: TextStyle(color: myColors.purple100),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    width: width / 4,
//                            padding: EdgeInsets.only(left: width/6, right: 20),
                    child: TextFormField(
                      validator: (v) {
                        if (v != null && v.length == 0) {
                          return "        " +
                              AppLocalizations.of(context).translate("Required");
                        }
                        return null;
                      },
                      onChanged: (v) {
                        try {
                          EditEventPageState.seats[widget.index].price =
                              int.parse(v);
                        } catch (e) {}
                      },
                      textAlign: TextAlign.center,
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly
                      ],
                      decoration: InputDecoration(
                          errorStyle: TextStyle(height: 0.05),
                          hintText: '20 SAR',
                          border: InputBorder.none,
                          hintStyle: TextStyle(color: myColors.purple69)),
                      controller: price,
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        border: Border.all(color: myColors.purple100)),
                  ),
                ],
              ),
            ],
          ),
          Column(
            children: [
              Text(
//                AppLocalizations.of(context).translate("Price Per Seat"),
                "Type",
                style: TextStyle(color: myColors.purple100),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: width / 2.5,
                height: 50,
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 6,right: 7),
                    child: Consumer<AddEventViewModel>(builder: (context, addEvent, child) {
                      return DropdownButtonFormField<String>(
                        value: EditEventPageState.seats[widget.index].type,
                        hint: Text(
                          'Type',
                          style: TextStyle(color: myColors.purple100),
                        ),
//                      isExpanded: true,
                        decoration: InputDecoration(
                            contentPadding: const EdgeInsets.all(0.0),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide.none,
                            ),
                            isDense: true),
                        onChanged: (salutation) {
//                        setState(() => type = salutation);
                          EditEventPageState.seats[widget.index].type = salutation;
                        },
                        validator: (value) =>
                        value == null ? "   "+AppLocalizations.of(context).translate("Required") : null,
                        items: addEvent.seats
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      );
                    },),
                  ),
                ),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    border: Border.all(color: myColors.purple100)),
              ),
            ],
          ),

        ],
      ),
    );
  }
}
