
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tonprovider/const/myColors.dart';



class MoveCamera extends StatefulWidget {
  final LatLng latLng;
   MoveCamera(this.latLng);
  @override
  State createState() => MoveCameraState();
}

class MoveCameraState extends State<MoveCamera> {
  Completer<GoogleMapController> _controller = Completer();

  static LatLng _center ;

  final Set<Marker> _markers = {};

  LatLng _lastMapPosition ;

  MapType _currentMapType = MapType.normal;


  @override
  void initState() {
    _center = widget.latLng;
    _lastMapPosition = _center;
    _markers.add(Marker(
      // This marker id can be anything that uniquely identifies each marker.
      markerId: MarkerId(_lastMapPosition.toString()),
      position: widget.latLng,
//        infoWindow: InfoWindow(
//          title: 'Really cool place',
//          snippet: '5 Star Rating',
//        ),
      icon: BitmapDescriptor.defaultMarker,
    ));
    super.initState();
  }


  void _onMapTypeButtonPressed() {
    setState(() {
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.satellite
          : MapType.normal;
    });
  }


  void _onCameraMove(CameraPosition position) {
    _lastMapPosition = position.target;
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(

        body: Stack(
          children: <Widget>[
            GoogleMap(
              onMapCreated: _onMapCreated,
              initialCameraPosition: CameraPosition(
                target: _center,
                zoom: 15.0,
              ),
              mapType: _currentMapType,
              markers: _markers,
              onCameraMove: _onCameraMove,
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Align(
                alignment: Alignment.topRight,
                child: Column(
                  children: <Widget> [
                    SizedBox(height: 60,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        FloatingActionButton(
                          onPressed: (){
                            Navigator.pop(context);
                          },
                          materialTapTargetSize: MaterialTapTargetSize.padded,
                          backgroundColor: myColors.purple100,
                          child: const Icon(Icons.arrow_back, size: 36.0),
                        ),
                        FloatingActionButton(
                          onPressed: _onMapTypeButtonPressed,
                          materialTapTargetSize: MaterialTapTargetSize.padded,
                          backgroundColor: myColors.purple100,
                          child: const Icon(Icons.map, size: 36.0),
                        ),

                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}