import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tonprovider/const/myColors.dart';
import 'package:tonprovider/screens/home/staffPage.dart';
import 'package:tonprovider/screens/home/homeBar.dart';
import 'package:tonprovider/screens/home/notifications.dart';
import 'package:tonprovider/screens/home/profilePage.dart';

import 'language/appLocalizations.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final PageController _pageController = PageController();
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: PageView(
        children: [
          HomeBar(),
          StaffPage(),
          NotificationPage(),
          ProfilePage(),
        ],
        controller: _pageController,
        onPageChanged: (p){
          setState(() {
            _selectedIndex = p;
          });
        },

      ),
      bottomNavigationBar: Container(

        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [
               myColors.orange69,
               myColors.purpleHomeBar,

//                    Color(0xffFCAF45),
//                        Color(0xffF58529),
              ]),
        ),
        child: BottomNavigationBar(
          showUnselectedLabels: true,
          unselectedItemColor:myColors.white,
          backgroundColor: Colors.transparent,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  "assets/icons/home.svg",
                  color: _selectedIndex == 0
                      ? myColors.deepPurple
                      : myColors.white,
                  height: 27,
                  width: 27,
                ),
                label: AppLocalizations.of(context).translate("Home")),
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  "assets/svg_icons/people.svg",
                  color: _selectedIndex == 1
                      ? myColors.deepPurple
                      : myColors.white,
                  height: 22,
                  width: 22,
                ),
                label: AppLocalizations.of(context).translate("Staff")),
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  "assets/icons/clarity_bell-outline-badged.svg",
                  color: _selectedIndex == 2
                      ? myColors.deepPurple
                      : myColors.white,
                  height: 19,
                  width: 20,
                ),
                label: AppLocalizations.of(context).translate("Notification")),
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  "assets/icons/profile.svg",
                  color: _selectedIndex == 3
                      ? myColors.deepPurple
                      : myColors.white,
                  height: 20,
                  width: 20,
                ),
                label: AppLocalizations.of(context).translate("Profile") ),
          ],

          onTap: _onTappedBar,
          selectedItemColor: myColors.deepPurple,
          currentIndex: _selectedIndex,
        ),
      ),
    );

  }


  void _onTappedBar(int value) {
      setState(() {
        _selectedIndex = value;
      });
      _pageController.jumpToPage(value);

  }
}
