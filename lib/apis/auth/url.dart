String get mainUrl {
  return 'https://toneap.com';
}

String login() {
  return '$mainUrl/auth/login';
}

String signup() {
  return '$mainUrl/auth/provider/signup';
}

String getEmployees(String id) {
  return '$mainUrl/auth/$id/employers';
}

String editEmployee(String id) {
  return '$mainUrl/auth/employer/$id';
}


String addEmployee() {
  return '$mainUrl/auth/provider/signup';
}

String getEvent(String providerID) {
  return '$mainUrl/event/provider/$providerID';
}

String getCategories() {
  return '$mainUrl/category';
}

String addEvent() {
  return '$mainUrl/event';
}

String editEvent(String id) {
  return '$mainUrl/event/$id';
}

String deleteEvent(String id) {
  return '$mainUrl/event/$id';
}

String uploadImage() {
  return '$mainUrl/upload';
}
String getProfileInfo() {
  return '$mainUrl/upload';
}
String checkBooking(String bookingId) {
  return '$mainUrl/booking/$bookingId';
}

String notification(String userId) {
  return '$mainUrl/notes/$userId';
}


String deleteEmployee(String employeeId) {
  return '$mainUrl/auth/employer/$employeeId';
}

String forgetPassword() {
  return '$mainUrl/auth/forgotPassword';
}
String providerInfo() {
  return '$mainUrl/auth/provider';
}

String auth() {
  return '$mainUrl/auth/provider';
}
String seats() {
  return '$mainUrl/seats';
}