import 'dart:convert';
import 'dart:io';
import 'package:tonprovider/view_models/home_view_model.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tonprovider/const/widgets.dart';
import 'package:tonprovider/apis/auth/url.dart'as url;
import 'package:tonprovider/language/appLocalizations.dart';
import 'package:tonprovider/view_models/user_view_model.dart';
import 'package:tonprovider/apis/api/api.dart' as api;
Future<bool> checkConnect(BuildContext context) async {
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      print('connected');
      return true;
    }
  } on SocketException catch (_) {
    print('not connected');
    showToast(AppLocalizations.of(context).translate("not connected"), context);
    return false;
  }
  return false;
}

Future<void> login(
    {BuildContext context, String email, String password}) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  Map<String, String> header = {'Content-Type': 'application/json'};
  var response;
  Uri uri = Uri.parse(url.login());
  response = await http.post(uri,
      body: jsonEncode({
        "email": email,
        "password": password,
        "fcm_token":await _firebaseMessaging.getToken()
      }),
      headers: header);
  final parsed = await jsonDecode(response.body);
  if (response.statusCode != HttpStatus.ok) {
    showToast(parsed['msg'], context,gravity: ToastGravity.CENTER);
    Navigator.pop(context);
    return;
  }

  prefs.setString("loginInfo", jsonEncode({
    "email": email,
    "password": password,
    "fcm_token":await _firebaseMessaging.getToken()
  }));

  prefs.setString("token", parsed["token"]);
  uri = Uri.parse(url.auth());
  header['x-auth-token'] = jsonDecode(response.body)['token'];
  await http.post(uri, headers: header);

  await api.getProfileInfo();
  prefs.setString("user", jsonEncode(parsed['user']));
  prefs.setBool("isLogin", true);

  Provider.of<HomeViewModel>(context,listen: false).getNotifies();
  Provider.of<UserViewModel>(context,listen: false).profileInfo();


  if(parsed['user']!=null&&parsed['user']['type']!=null&&parsed['user']['type']=="employer"){
    prefs.setString("providerID", parsed['user']['provider_id']);
    prefs.setString("providerName", parsed['user']['name']??"");
    prefs.setBool("isProvider", false);
    Navigator.popUntil(
      context,
      ModalRoute.withName('/'),
    );
    Navigator.pushNamed(context, '/HomeEmployeePage');
  }
  else{
    prefs.setBool("isProvider", true);
    prefs.setString("providerID", parsed['user']['_id']);
    prefs.setString("providerName", parsed['user']['name']);

    Navigator.popUntil(
      context,
      ModalRoute.withName('/'),
    );
    Navigator.pushNamed(context, '/HomePage');
  }

}




Future<void> forgetPassword({String email, BuildContext context}) async {
  Map<String, String> header = {'Content-Type': 'application/json'};
  Uri uri = Uri.parse(url.forgetPassword());
  var response =
  await http.post(uri, headers: header, body: jsonEncode({"email": email}));
  final parsed = await jsonDecode(response.body);
  if (response.statusCode != HttpStatus.ok) {
    showToast(parsed['msg'], context,gravity: ToastGravity.CENTER);
    Navigator.pop(context);
    return;
  }
  showToast(parsed['message'], context,gravity: ToastGravity.CENTER);
  Navigator.pop(context);
  return;
}