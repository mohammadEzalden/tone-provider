import 'dart:convert';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tonprovider/apis/auth/url.dart' as url;
import 'package:tonprovider/const/widgets.dart';
import 'package:tonprovider/view_models/user_view_model.dart';
import 'package:tonprovider/apis/api/api.dart' as api;
import 'package:tonprovider/view_models/home_view_model.dart';
Future<void> signup(
    {String username,
    String phone,
    String email,
    String password,
    BuildContext context}) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  Map<String, String> header = {'Content-Type': 'application/json'};
  var response;
  Uri uri = Uri.parse(url.signup());
  response = await http.post(uri,
      body: jsonEncode({
        "name": username,
        "email": email,
        "phone": phone,
        "password": password,
        "fcm_token":await _firebaseMessaging.getToken()
      }),
      headers: header);
  final parsed = await jsonDecode(response.body);
  if (response.statusCode != HttpStatus.ok) {
    showToast(parsed['msg'], context);
    Navigator.pop(context);
    return;
  }
  prefs.setString("loginInfo", jsonEncode({
    "email": email,
    "password": password,
    "fcm_token":await _firebaseMessaging.getToken()
  }));

  prefs.setString("token", parsed["token"]);
  uri = Uri.parse(url.auth());
  header['x-auth-token'] = jsonDecode(response.body)['token'];
  await http.post(uri, headers: header);
  prefs.setBool("isLogin", true);
  prefs.setString("user", jsonEncode(parsed['user']));
  prefs.setBool("isProvider", true);
  prefs.setString("providerID", parsed['user']['_id']);
  await api.getProfileInfo();
  Provider.of<HomeViewModel>(context,listen: false).getNotifies();
  Provider.of<UserViewModel>(context,listen: false).profileInfo();




  Navigator.popUntil(
    context,
    ModalRoute.withName('/'),
  );
  Navigator.pushNamed(context, '/HomePage');
}
