import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:tonprovider/apis/auth/url.dart' as url;
import 'package:tonprovider/const/widgets.dart';
import 'package:tonprovider/model/employee.dart';
import 'package:tonprovider/model/event.dart';
import 'package:tonprovider/model/profile.dart';
import 'package:tonprovider/model/user.dart';
import 'package:tonprovider/view_models/home_view_model.dart';

Future<List<Employee>> getEmployees() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();

  Map<String, String> header = {'Content-Type': 'application/json'};
  var response;
  String providerId = prefs.getString("providerID");

  Uri uri = Uri.parse(url.getEmployees(providerId));

  response = await http.get(uri, headers: header);
  final parsed = await jsonDecode(response.body);
  if (response.statusCode != HttpStatus.ok) {
    return [];
  }
  return (parsed as List)
      .map<Employee>((json) => Employee.fromJson(json))
      .toList();
}

Future<Employee> addEmployees(
    {String email, String password, BuildContext context}) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();

  Map<String, String> header = {'Content-Type': 'application/json'};
  var response;
  String providerId = prefs.getString("providerID");
  Uri uri = Uri.parse(url.addEmployee());

  response = await http.post(uri,
      headers: header,
      body: jsonEncode({
        "provider_id": providerId,
        "email": email,
        "password": password,
        "type": "employer",
        "roles": {
          "edit_event": false,
          "add_event": false,
          "QrcodeScan": false,
          "edit_provider_info": false,
          "employers_control": false
        }
      }));
  final parsed = await jsonDecode(response.body);
  if (response.statusCode != HttpStatus.ok) {
    showToast(parsed['msg'], context, gravity: ToastGravity.CENTER);
    Navigator.pop(context);
    return null;
  }
  Provider.of<HomeViewModel>(context, listen: false).getNotifies();

  Navigator.pop(context);

  return Employee.fromJson(parsed['user']);
}

Future<Employee> editEmployee({Employee e, BuildContext context}) async {
  Map<String, String> header = {'Content-Type': 'application/json'};
  var response;
  Uri uri = Uri.parse(url.editEmployee(e.id));

  response = await http.put(uri, headers: header, body: jsonEncode(e));
  //60ad4784eab02e0022506fe8
  final parsed = await jsonDecode(response.body);
  if (response.statusCode != HttpStatus.ok) {
    showToast(parsed['msg'], context, gravity: ToastGravity.CENTER);
    Navigator.pop(context);
    return null;
  }
  Provider.of<HomeViewModel>(context, listen: false).getNotifies();

  Navigator.pop(context);
  return Employee.fromJson(parsed);
}

Future<bool> deleteEmployee({String id, BuildContext context}) async {
  Map<String, String> header = {'Content-Type': 'application/json'};
  var response;
  Uri uri = Uri.parse(url.deleteEmployee(id));

  response = await http.delete(uri, headers: header);
  if (response.statusCode != HttpStatus.ok) {
    final parsed = await jsonDecode(response.body);
    showToast(parsed['msg'], context, gravity: ToastGravity.CENTER);
    Navigator.pop(context);
    return false;
  }
  Provider.of<HomeViewModel>(context, listen: false).getNotifies();

  Navigator.pop(context);
  return true;
}

Future<Event> addEvent({Event event, File image, BuildContext context}) async {
  String imageUrl = await uploadImage(image);
  SharedPreferences prefs = await SharedPreferences.getInstance();

  Map<String, String> header = {'Content-Type': 'application/json'};
  var response;
  String providerId = prefs.getString("providerID");

  Uri uri = Uri.parse(url.addEvent());

  Map<String, dynamic> json = event.toJson();

  json['provider_id'] = providerId;
  json['provider_name'] = prefs.getString("providerName") ?? "Test";
  json["imageUrl"] = imageUrl;
  print("___1__________________2");

  print(json);
  response = await http.post(uri, headers: header, body: jsonEncode(json));
  final parsed = await jsonDecode(response.body);
  if (response.statusCode != HttpStatus.ok) {
    print("_____________________2");
    showToast(parsed['msg'], context, gravity: ToastGravity.CENTER);
    Navigator.pop(context);

    return null;
  }
  Provider.of<HomeViewModel>(context, listen: false).getNotifies();

  Navigator.pop(context);

  print("_____________________1");
  return Event.fromJson(parsed);
}

Future<Event> editEvent({Event event, File image, BuildContext context}) async {
  String imageUrl;
  if (image != null) {
    imageUrl = await uploadImage(image);
  } else
    imageUrl = event.image;

  Map<String, String> header = {'Content-Type': 'application/json'};
  var response;

  Uri uri = Uri.parse(url.editEvent(event.id));

  Map<String, dynamic> json = event.toJson();
  json["imageUrl"] = imageUrl;
  response = await http.put(uri, headers: header, body: jsonEncode(json));
  final parsed = await jsonDecode(response.body);
  if (response.statusCode != HttpStatus.ok) {
    showToast(parsed['msg'], context, gravity: ToastGravity.CENTER);
    Navigator.pop(context);
    return null;
  }
  Provider.of<HomeViewModel>(context, listen: false).getNotifies();

  Navigator.pop(context);

  return Event.fromJson(parsed);
}

Future<bool> deleteEvent({String id, BuildContext context}) async {
  Map<String, String> header = {'Content-Type': 'application/json'};
  var response;

  Uri uri = Uri.parse(url.deleteEvent(id));

  print(id);
  response = await http.delete(uri, headers: header);
  if (response.statusCode != HttpStatus.ok) {
    final parsed = await jsonDecode(response.body);
    showToast(parsed['msg'], context, gravity: ToastGravity.CENTER);
    Navigator.pop(context);
    return false;
  }
  Provider.of<HomeViewModel>(context, listen: false).getNotifies();
  Navigator.pop(context);

  return true;
}

Future<String> uploadImage(File file) async {
  //create multipart request for POST or PATCH method

  var request = http.MultipartRequest("POST", Uri.parse(url.uploadImage()));
  var pic = await http.MultipartFile.fromPath("file", file.path);
  //add multipart to request
  request.files.add(pic);
  var response = await request.send();

  var responseData = await response.stream.toBytes();
  var responseString = String.fromCharCodes(responseData);
  print("+++++++2");
  print("+++++++2"+responseString);
  return jsonDecode(responseString)['imageUrl'];
}

Future<void> getProfileInfo() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();

  Map<String, String> header = {
    'Content-Type': 'application/json',
    "x-auth-token": prefs.getString("token")
  };
  print("_______________3");

  var response;
  Uri uri = Uri.parse(url.providerInfo());

  response = await http.get(uri, headers: header);
  final parsed = await jsonDecode(response.body);
  print(parsed);
  if (response.statusCode != HttpStatus.ok) {
    return null;
  }
  prefs.setString("user", jsonEncode(parsed));
}

Future<Map<String,dynamic>> checkBooking(String bookingId) async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  Map<String, String> header = {
    'Content-Type': 'application/json',
    'x-auth-token': prefs.getString("token")
  };
  var response;
  Uri uri = Uri.parse(url.checkBooking(bookingId));

  try {
    response = await http.get(uri, headers: header);
    if (response.statusCode != HttpStatus.ok) {
      return {};
    }
  } catch (e) {
    return {};
  }

  final parsed = await jsonDecode(response.body); //seatType
  return {
    "numberOfSeats": parsed["numberOfSeats"],
    "seatType": parsed["seatType"],
  };
}

Future<User> editUser({User u, File image}) async {
  if (image != null) u.imageUrl = await uploadImage(image);
  Map<String, String> header = {'Content-Type': 'application/json'};
  Uri uri = Uri.parse(url.editEmployee(u.id));
  var response = await http.put(uri, headers: header, body: jsonEncode(u));
  if (response.statusCode != HttpStatus.ok) {
    return null;
  }
  final parsed = await jsonDecode(response.body);
  return User.fromJson(parsed);
}

void readNotification() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  Map<String, String> header = {'Content-Type': 'application/json'};
  Uri uri = Uri.parse(url.notification(prefs.getString("providerID")));
  var response = await http.put(uri, headers: header);
}
