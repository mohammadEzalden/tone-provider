import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tonprovider/const/myColors.dart';
import 'package:tonprovider/language/appLocalizations.dart';
import 'package:tonprovider/view_models/user_view_model.dart';

class CustomDrawer extends StatefulWidget {
  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  Future<void> share(String url) async {
    String _uri = Uri.encodeFull(url);
    //TODO share link
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    return Drawer(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Consumer<UserViewModel>(
          builder: (context, user, child) {
            if (!user.employee.role.edit_provider_info &&
                !user.employee.role.employers_control &&
                !user.employee.role.add_event &&
                !user.employee.role.edit_event &&
                !user.employee.role.QrcodeScan) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.start,

                children: [
                  SizedBox(
                    height: 105,
                  ),
                  SvgPicture.asset(
                    "assets/icons/tone.svg",
                    color: myColors.purple100,
                  ),
                  SizedBox(
                    height: 100,
                  ),
                  Text(AppLocalizations.of(context).translate("You don't have any permission")),
                  SizedBox(height: 50,),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 30),
                    child: InkWell(
                      onTap: () async {
                        showDialog(
                            context: context,
                            builder: (BuildContext cxt) {
                              return AlertDialog(
                                title: Text(
                                  AppLocalizations.of(context).translate("Alert"),
                                  style: TextStyle(
                                      color: myColors.purple100, fontWeight: FontWeight.bold),
                                ),
                                content: StatefulBuilder(
                                  builder: (BuildContext context, StateSetter setState) {
                                    return Container(
                                      child: SingleChildScrollView(
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(AppLocalizations.of(context).translate("Are you sure you want to Log out?"),style: TextStyle(color: myColors.purple100),),
                                            SizedBox(
                                              height: 20,
                                            ),
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              children: [
                                                InkWell(
                                                  onTap: () {
                                                    Navigator.pop(context);
                                                  },
                                                  child: Container(
                                                    width: width / 4,
                                                    padding: EdgeInsets.only(
                                                        left: 8, right: 8, top: 12, bottom: 12),
                                                    child: Center(
                                                      child: Text(
                                                        AppLocalizations.of(context).translate("Discard"),
                                                        style: TextStyle(
                                                            color: myColors.purple100,
                                                            fontWeight: FontWeight.bold),
                                                      ),
                                                    ),
                                                    decoration: BoxDecoration(
                                                        borderRadius: BorderRadius.circular(30),
                                                        border:
                                                        Border.all(color: myColors.purple100)),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                RawMaterialButton(
                                                  fillColor: myColors.purple100,
                                                  splashColor: myColors.purple100,
                                                  shape: StadiumBorder(
                                                      side: BorderSide(color: myColors.purple100)),
                                                  child: Padding(
                                                    padding: EdgeInsets.symmetric(
                                                        horizontal: width / 19, vertical: 15.0),
                                                    child: Text(
                                                      AppLocalizations.of(context).translate("Log out"),
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontWeight: FontWeight.bold),
                                                    ),
                                                  ),
                                                  onPressed: ()async {
                                                    SharedPreferences prefs = await SharedPreferences.getInstance();
                                                    prefs.clear();
                                                    Navigator.pop(context);
                                                    Navigator.pop(context);
                                                    Navigator.pushReplacementNamed(context, '/LoginPage');
                                                  },
                                                ),
                                              ],
                                            ),

                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                ),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0)), //this right here
                              );
                            });

                      },
                      child: Row(
                        children: [
                          SizedBox(
                            width: 44,
                          ),
                          Icon(
                            Icons.logout,
                            color: myColors.purple100,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 24),
                            child: Text(
                              AppLocalizations.of(context).translate("Log out"),
                              style:
                              TextStyle(fontSize: 18, color: myColors.purple100),
                            ),
                          ),

                        ],
                      ),
                    ),
                  ),

                ],);
            }
            return Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  height: 105,
                ),
                SvgPicture.asset(
                  "assets/icons/tone.svg",
                  color: myColors.purple100,
                ),
                SizedBox(
                  height: 100,
                ),
                user.employee.role.QrcodeScan
                    ? Padding(
                        padding: const EdgeInsets.only(bottom: 30),
                        child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.pushNamed(context, '/QRCodeScanPage');
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset(
                                "assets/svg_icons/Icon ionic-ios-qr-scanner.svg",
                                color: myColors.purple100,
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Text(
                                AppLocalizations.of(context).translate("QR Code Scanning"),
                                style: TextStyle(
                                    color: myColors.purple100, fontSize: 17),
                              )
                            ],
                          ),
                        ),
                      )
                    : Container(),
                user.employee.role.edit_event || user.employee.role.add_event
                    ? Padding(
                        padding: const EdgeInsets.only(bottom: 30),
                        child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset(
                                "assets/icons/bi_calendar-event.svg",
                                color: myColors.purple100,
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Text(
                                AppLocalizations.of(context).translate("Event Management"),
                                style: TextStyle(
                                    color: myColors.purple100, fontSize: 17),
                              )
                            ],
                          ),
                        ),
                      )
                    : Container(),
                user.employee.role.employers_control
                    ? Padding(
                        padding: const EdgeInsets.only(bottom: 30),
                        child: InkWell(
                          onTap: () {
                            Navigator.pop(context);

                            Navigator.pushNamed(context, '/StaffPage');

                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset(
                                "assets/svg_icons/service.svg",
                                color: myColors.purple100,
                                height: 21,
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Text(
                                AppLocalizations.of(context).translate("Staff Management"),
                                style: TextStyle(
                                    color: myColors.purple100, fontSize: 17),
                              )
                            ],
                          ),
                        ),
                      )
                    : Container(),
//                user.employee.role.edit_provider_info
//                    ? Padding(
//                        padding: const EdgeInsets.only(bottom: 30),
//                        child: InkWell(
//                          onTap: () {
//                            Navigator.pop(context);
//
//                            Navigator.pushNamed(context, '/ProfilePage');
//                          },
//                          child: Row(
//                            mainAxisAlignment: MainAxisAlignment.center,
//                            children: [
//                              SvgPicture.asset(
//                                "assets/svg_icons/gg_profile-1.svg",
//                                color: myColors.purple100,
//                              ),
//                              SizedBox(
//                                width: 20,
//                              ),
//                              Text(
//                                "Provider Management",
//                                style: TextStyle(
//                                    color: myColors.purple100, fontSize: 17),
//                              )
//                            ],
//                          ),
//                        ),
//                      )
//                    : Container(),
                Padding(
                  padding: const EdgeInsets.only(bottom: 30),
                  child: InkWell(
                    onTap: () async {
                      showDialog(
                          context: context,
                          builder: (BuildContext cxt) {
                            return AlertDialog(
                              title: Text(
                                AppLocalizations.of(context).translate("Alert"),
                                style: TextStyle(
                                    color: myColors.purple100, fontWeight: FontWeight.bold),
                              ),
                              content: StatefulBuilder(
                                builder: (BuildContext context, StateSetter setState) {
                                  return Container(
                                    child: SingleChildScrollView(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(AppLocalizations.of(context).translate("Are you sure you want to Log out?"),style: TextStyle(color: myColors.purple100),),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            children: [
                                              InkWell(
                                                onTap: () {
                                                  Navigator.pop(context);
                                                },
                                                child: Container(
                                                  width: width / 4,
                                                  padding: EdgeInsets.only(
                                                      left: 8, right: 8, top: 12, bottom: 12),
                                                  child: Center(
                                                    child: Text(
                                                      AppLocalizations.of(context).translate("Discard"),
                                                      style: TextStyle(
                                                          color: myColors.purple100,
                                                          fontWeight: FontWeight.bold),
                                                    ),
                                                  ),
                                                  decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.circular(30),
                                                      border:
                                                      Border.all(color: myColors.purple100)),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              RawMaterialButton(
                                                fillColor: myColors.purple100,
                                                splashColor: myColors.purple100,
                                                shape: StadiumBorder(
                                                    side: BorderSide(color: myColors.purple100)),
                                                child: Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: width / 19, vertical: 15.0),
                                                  child: Text(
                                                    AppLocalizations.of(context).translate("Log out"),
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontWeight: FontWeight.bold),
                                                  ),
                                                ),
                                                onPressed: ()async {
                                                  SharedPreferences prefs = await SharedPreferences.getInstance();
                                                  prefs.clear();
                                                  Navigator.pop(context);
                                                  Navigator.pop(context);
                                                  Navigator.pushReplacementNamed(context, '/LoginPage');
                                                },
                                              ),
                                            ],
                                          ),

                                        ],
                                      ),
                                    ),
                                  );
                                },
                              ),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15.0)), //this right here
                            );
                          });

                    },
                    child: Row(
                      children: [
                        SizedBox(
                          width: 44,
                        ),
                        Icon(
                          Icons.logout,
                          color: myColors.purple100,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 24),
                          child: Text(
                            AppLocalizations.of(context).translate("Log out"),
                            style:
                            TextStyle(fontSize: 18, color: myColors.purple100),
                          ),
                        ),

                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
