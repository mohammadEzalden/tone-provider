import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tonprovider/StaffPart/cutomeDrawer.dart';
import 'package:tonprovider/const/myColors.dart';
import 'package:tonprovider/customWidget/image_from_network.dart';
import 'package:tonprovider/language/appLocalizations.dart';
import 'package:tonprovider/screens/home/eventDetails.dart';
import 'package:tonprovider/view_models/home_view_model.dart';
import 'package:tonprovider/view_models/user_view_model.dart';

class HomeEmployeePage extends StatefulWidget {
  @override
  _HomeEmployeePageState createState() => _HomeEmployeePageState();
}

class _HomeEmployeePageState extends State<HomeEmployeePage> {
  final TextEditingController code = TextEditingController();
  String category = "all";
  DateFormat fDay;

  DateFormat fDate;

  DateFormat fTime;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();
  @override
  void initState() {
    super.initState();
    if (Platform.isIOS) iOS_Permission();
    const AndroidInitializationSettings initializationSettingsAndroid =
    AndroidInitializationSettings('app_icon');
    final IOSInitializationSettings initializationSettingsIOS =
    IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    final MacOSInitializationSettings initializationSettingsMacOS =
    MacOSInitializationSettings();
    final InitializationSettings initializationSettings =
    InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS,
        macOS: initializationSettingsMacOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print(message);


        const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails('your channel id', 'your channel name',
            'your channel description',
            importance: Importance.max,
            priority: Priority.high,
            showWhen: false);
        const NotificationDetails platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);
        await flutterLocalNotificationsPlugin.show(
            0,
            message['notification']['title'],
            message['notification']['body'],
            platformChannelSpecifics,
            payload: 'shopper');
        Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
        Provider.of<HomeViewModel>(context, listen: false).getNotifies();
      },
      onLaunch: (Map<String, dynamic> message) async {
        print(message);
        Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();

        Provider.of<HomeViewModel>(context, listen: false).getNotifies();
      },
      onResume: (Map<String, dynamic> message) async {
        Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
        Provider.of<HomeViewModel>(context, listen: false).getNotifies();

        print(message);
      },
    );

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  Future selectNotification(String payload) async {
    Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
    Provider.of<HomeViewModel>(context, listen: false).getNotifies();
    if (payload != null) {
      debugPrint('notification payload: $payload');
    }
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
    Provider.of<HomeViewModel>(context, listen: false).getNotifies();
  }

  Future<dynamic> onSelectNotification(String payload) async {
    Provider.of<UserViewModel>(context, listen: false).updateProfileInfo();
    Provider.of<HomeViewModel>(context, listen: false).getNotifies();
  }
  @override
  Widget build(BuildContext context) {
    fDay = new DateFormat("EEEE", AppLocalizations.of(context).locale.languageCode);
    fDate = new DateFormat(
        'yyyy-MM-dd', AppLocalizations.of(context).locale.languageCode);
    fTime = new DateFormat("jm",AppLocalizations.of(context).locale.languageCode);
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      key: _scaffoldKey,
      drawer: CustomDrawer(),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0), // here the desired height
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
              decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.topRight,
                colors: [
                  myColors.orange,
                  myColors.purple100,
                ]),
          )),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(left:12,right:12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: height / 29,
                  ),
                  Row(
                    children: [
                      IconButton(
                          icon: Icon(
                            Icons.menu,
                            size: 33,
                            color: myColors.purple100,
                          ),
                        onPressed: () => _scaffoldKey.currentState.openDrawer(),),
                      SvgPicture.asset(
                        "assets/icons/tone.svg",
                        color: myColors.purple100,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: height/29,
                  ),
                  Container(
                    width: width -16,
                    child: TextFormField(
                      validator: (v) {
                        if (v != null && v.length == 0) {
                          return "يرجى ادخال رمز التفعيل";
                        }
                        return null;
                      },
                      textInputAction: TextInputAction.search,
                      decoration: InputDecoration(
                          prefixIcon: IconButton(
                            icon: SvgPicture.asset(
                              "assets/icons/search.svg",
                              height: 18,
                              width: 18,
                            ),
                            onPressed: () {},
                          ),
                          suffixIcon: IconButton(
                            icon: SvgPicture.asset(
                              "assets/icons/filter.svg",
                              height: 19,
                              width: 20,
                            ),
                            onPressed: () {},
                          ),
                          hintText: AppLocalizations.of(context).translate("Search"),
                          border: InputBorder.none,
                          hintStyle: TextStyle(color: myColors.purple69)),
                      controller: code,
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        border: Border.all(color: myColors.gray)),
                  ),
                  SizedBox(
                    height: height/29,
                  ),
                  Text(
                    AppLocalizations.of(context).translate("Your Events"),
                    style: TextStyle(fontSize: 18, color: myColors.purple100),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                ],
              ),
            ),
            Consumer<HomeViewModel>(builder: (context, home, child) {
              if (home.eventsLoading)
                return Center(
                  child: CircularProgressIndicator(),
                );
              if (home.events.isEmpty)
                return Padding(
                  padding: EdgeInsets.only(top: height / 4),
                  child: Center(
                    child: Text(
                      AppLocalizations.of(context)
                          .translate("You Don't have any Event"),
                      style: TextStyle(color: Colors.grey, fontSize: 20),
                    ),
                  ),
                );
              return Padding(
                padding: EdgeInsets.only(left: 12,right: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: home.events
                      .map((e) => InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => EventDetailsPage(
                                            e: e,
                                          )));
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(top: 20),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    height: height / 7,
                                    width: width / 2.1,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: myColors.purple100,
                                    ),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(8.0),
                                      child: ImageFromNetworkWidget(e.image),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 16,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        e.name,
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold,
                                            color: myColors.purple100),
                                      ),
                                      SizedBox(
                                        height: 8,
                                      ),
                                      Text(
                                        fDay.format(e.date) +
                                            "\n" +
                                            fDate.format(e.date),
                                        style: TextStyle(
                                            color: myColors.purple100),
                                      ),
                                      Text(
                                          AppLocalizations.of(context).translate("at")+" " + fTime.format(e.date),
                                        style: TextStyle(
                                            color: myColors.purple100),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ))
                      .toList(),
                ),
              );
            }),
            SizedBox(
              height: 100,
            )
          ],
        ),
      ),
      floatingActionButton: Consumer<UserViewModel>(
        builder: (context, user, child) {
          if (user.isLogin) return Container();
          return user.employee.role.add_event
              ? TextButton(
              style: ButtonStyle(
                  padding: MaterialStateProperty.all(
                      EdgeInsets.symmetric(horizontal: width / 9, vertical: height/59)),
                  backgroundColor: MaterialStateProperty.all(myColors.purple100),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(color: Colors.grey[300])))),
              onPressed: () {
                Navigator.pushNamed(context, "/AddEventPage");
              },
              child: Text(
                AppLocalizations.of(context).translate("Add New Event"),
                style: TextStyle(color: myColors.white),
              ))
              : Container();
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,

    );
  }
}
