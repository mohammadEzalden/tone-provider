import 'dart:io';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_svg/svg.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:tonprovider/StaffPart/cutomeDrawer.dart';
import 'package:tonprovider/const/myColors.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:tonprovider/const/widgets.dart';
import 'package:tonprovider/apis/api/api.dart' as api;
import 'package:tonprovider/language/appLocalizations.dart';

class QRCodeScanPage extends StatefulWidget {
  @override
  _QRCodeScanPageState createState() => _QRCodeScanPageState();
}

const flashOn = 'FLASH ON';
const flashOff = 'FLASH OFF';
const frontCamera = 'FRONT CAMERA';
const backCamera = 'BACK CAMERA';

class _QRCodeScanPageState extends State<QRCodeScanPage> {
  final TextEditingController code = TextEditingController();
  String category = "all";
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Barcode result;
  var flashState = flashOn;
  var cameraState = frontCamera;
  QRViewController controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller.pauseCamera();
    } else if (Platform.isIOS) {
      controller.resumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      key: _scaffoldKey,
      drawer: CustomDrawer(),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0), // here the desired height
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
              decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.topRight,
                colors: [
                  myColors.orange,
                  myColors.purple100,
                ]),
          )),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(left: 12, right: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: height / 29,
                  ),
                  Row(
                    children: [
//                        icon: Icon(
//                          Icons.menu,
//                          size: 33,
//                          color: myColors.purple100,
//                        ),
//                        onPressed: () => _scaffoldKey.currentState.openDrawer(),),
                      SvgPicture.asset(
                        "assets/icons/tone.svg",
                        color: myColors.purple100,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: height / 29,
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Row(
                      children: [
                        Icon(
                          Icons.arrow_back,
                          color: myColors.purple100,
                        ),
                        Text(
                          AppLocalizations.of(context)
                              .translate("Scan QR Code"),
                          style: TextStyle(
                              fontSize: 18, color: myColors.purple100),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: height / 29,
                  ),
                  Container(
                    padding: EdgeInsets.all(7),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(color: myColors.purple100, spreadRadius: 1),
                      ],
                    ),
                    height: height / 2.2,
                    width: width - 16,
                    child: _buildQrView(context),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: height / 29,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButton(
                    icon: Icon(
                      _isFlashOn(flashState) ? Icons.flash_on : Icons.flash_off,
                      color: myColors.purple100,
                    ),
                    onPressed: () {
                      if (controller != null) {
                        controller.toggleFlash();
                        if (_isFlashOn(flashState)) {
                          setState(() {
                            flashState = flashOff;
                          });
                        } else {
                          setState(() {
                            flashState = flashOn;
                          });
                        }
                      }
                    }),
                SizedBox(
                  width: width / 6,
                ),
                InkWell(
                  onTap: () {
                    if (result != null) String res = result.code;
                    controller?.resumeCamera();
//                  controller?.pauseCamera();
                  },
                  child: Container(
                    padding: EdgeInsets.all(3),
                    width: 81.0,
                    height: 81.0,
                    decoration: new BoxDecoration(
                      color: myColors.purple100,
                      shape: BoxShape.circle,
                    ),
                    child: Container(
                      padding: EdgeInsets.all(5),
                      decoration: new BoxDecoration(
                        color: myColors.white,
                        shape: BoxShape.circle,
                      ),
                      child: Container(
                        decoration: new BoxDecoration(
                          color: Colors.grey,
                          shape: BoxShape.circle,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: width / 3.5,
                ),
              ],
            ),
//            SizedBox(
//              height: height / 20,
//            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.center,
//              children: [
//                TextButton(
//                    style: ButtonStyle(
//                        padding: MaterialStateProperty.all(
//                            EdgeInsets.symmetric(horizontal: 50, vertical: 15)),
//                        backgroundColor:
//                            MaterialStateProperty.all(myColors.purple100),
//                        shape:
//                            MaterialStateProperty.all<RoundedRectangleBorder>(
//                                RoundedRectangleBorder(
//                                    borderRadius: BorderRadius.circular(18.0),
//                                    side:
//                                        BorderSide(color: Colors.grey[300])))),
//                    onPressed: () {showResult(-1);},
//                    child: Text(
//                      "Submit",
//                      style: TextStyle(color: myColors.white),
//                    )),
//              ],
//            )
          ],
        ),
      ),
    );
  }

  bool _isFlashOn(String current) {
    return flashOn == current;
  }

  bool _isBackCamera(String current) {
    return backCamera == current;
  }

  Widget _buildQrView(BuildContext context) {
    // For this example we check how width or tall the device is and change the scanArea and overlay accordingly.
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
            MediaQuery.of(context).size.height < 400)
        ? 270.0
        : 300.0;
    // To ensure the Scanner view is properly sizes after rotation
    // we need to listen for Flutter SizeChanged notification and update controller
    return NotificationListener<SizeChangedLayoutNotification>(
        onNotification: (notification) {
          Future.microtask(
              () => controller?.updateDimensions(qrKey, scanArea: scanArea));
          return false;
        },
        child: SizeChangedLayoutNotifier(
            key: const Key('qr-size-notifier'),
            child: QRView(
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
              overlay: QrScannerOverlayShape(
                borderColor: myColors.purple100,
                borderRadius: 10,
                borderLength: 30,
                borderWidth: 10,
                cutOutSize: scanArea,
              ),
            )));
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) async {
      setState(() {
        controller?.pauseCamera();
      });
      result = scanData;
      buildShowDialog(context);
      Map<String, dynamic> res =
          await api.checkBooking(result.code).whenComplete(() {
        Navigator.pop(context);
      });
      if (res != null) showResult(res);
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  showResult(Map<String, dynamic> res) {
    String msg = res['numberOfSeats'] != null
        ? AppLocalizations.of(context).translate("Welcome, you have") +
            " ${res['numberOfSeats']} ${res['seatType']}" +
            AppLocalizations.of(context).translate("sits")
        : AppLocalizations.of(context)
            .translate("Sorry you aren't into the Event");
    showDialog(
        context: context,
        barrierColor: Color(0xBFFFFFFF),
        builder: (BuildContext context) {
          return AlertDialog(
            content: Container(
              height: 200,
              child: Column(
                children: [
                  res['numberOfSeats'] == null
                      ? SvgPicture.asset("assets/icons/wrong.svg")
                      : SvgPicture.asset("assets/icons/true.svg"),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    msg,
                    style: TextStyle(
                        color: res['numberOfSeats'] != null
                            ? Colors.green
                            : Colors.red),
                  )
                ],
              ),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0)), //this right here
          );
        });
  }
}
